from mutt import client
from mutt.common import AdType, is_attr_none
import pytest
import uuid
import mutt.request.request_mutt as mrp
from functools import reduce
from mutt.config import environment as mutt_env
from aerserv.config.aerserv import environment as as_env
from aerserv import whitelist_params
from mutt.request.request_ua import ExtInfo4UARequest
from mutt.request.request_mutt import micro
"""
UMP has a default rev_share: 0.57

ticket: https://aerserv.atlassian.net/browse/DEV-5197
"""


def is_adv_price_as_expected(adv_price_from_ua: list, ad_price_expected: list):
    if len(adv_price_from_ua) != len(ad_price_expected):
        return False
    print([abs(a - b) <= 0.01 for a, b in zip(adv_price_from_ua, ad_price_expected)])
    return all([abs(a - b) <= 0.01 for a, b in zip(adv_price_from_ua, ad_price_expected)])


@pytest.mark.flaky(reruns=2)
@pytest.mark.parametrize(
    "plc_desc, as_plcid, adid, expected_ads_from_mutt, expected_prices",
    [
        (
            "Android - Multiple RTB Insterstial Static Bids(at=1) + Dynamic SDK Ad",
            380988,
            "test_5_static_bids_with_price_at_4_5_6_7_8",
            2,
            [4.81, 8.0]  # rev_share=0.6
        ),
        (
            "iOS - Multiple RTB Interstital Vast Bids(at=2) + 2 Dynamic SDK Ads",
            380989,
            "test_5_vast_bids_at_4_5_7_10_14",
            6,
            [8.41, 10.01, 8.343, 4.21] #[8.41, 10.19, 8.523, 4.21]  # rev_share=0.6
        ),
        (
            "iOS - 1 Non-Dynamic Interstital Vast SDK + 2 Dynamic Interstitial Vast SDK",
            380744,
            None,
            3,
            [10.0, 8.0, 5.0]  # no rev_share
        ),
        (
            "Android - 1 Non-Dynamic Interstital Vast SDK + 2 Dynamic Interstitial Vast SDK",
            380744,
            None,
            3,
            [10.0, 8.0, 5.0]  # no rev_share
        ),
    ]
)
def test_ua_response_for_as(
    plc_desc: str,
    as_plcid: int,
    adid: str or None,
    expected_ads_from_mutt: int,
    expected_prices: list,
):
    adid = (adid or str(uuid.uuid4()))
    print(f"cache_key: {as_plcid}_{adid}_{whitelist_params.AD_CACHING_METRO_CODE}")
    os = "iOS" if "iOS" in plc_desc else "Android"

    mutt_response, ua_response = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        as_plid=as_plcid,
        os=os,
        adid=adid,
        has_dynamic_mediation=True,
        https=True,
    )
    assert len(mutt_response) == expected_ads_from_mutt
    assert mutt_response.ad_sets[0].isAuctionClosed == False
    assert is_adv_price_as_expected([float(ad.macros["${advPrice}"]) for ad in ua_response.ads], expected_prices)


@pytest.mark.flaky(reruns=2)
@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, a_bid, appnext_price, expected_prices",
    [
        ("Android - Winning UMP + AdColony + AppNext", 380999, 1539906510215, micro(20), None, [20.0]),
        ("Android - Winning UMP + AdColony + Dynamic Priced AppNext", 380999, 1539906510215, micro(20), None, [20.0]),
        ("Android - Losing UMP + AdColony + AppNext", 380999, 1539906510215, micro(3), None, [4.51, 4.5]),
        # TODO add dynamic price for AppNext
        ("Android - Losing UMP + AdColony + AppNext", 380999, 1539906510215, micro(20), 12, [12, 4.01]),
        ("iOS - Winning UMP + AdColony + Vungle", 380998, 1535339526186, micro(20), None, [20.0]),
        ("iOS - Losing UMP + AdColony + Vungle", 380998, 1535339526186, micro(3), None, [8.01, 4.01]),
    ]
)
def test_ua_response_for_ump(
    plc_desc: str, as_plid: int, im_plid: int, a_bid: int, appnext_price: int or None, expected_prices: list
):
    """
    all the as plcs have rev_share=0.57, just as UMP's default rev_share
    """
    adid = str(uuid.uuid4())
    print(f"cache_key: {as_plid}_{adid}_{whitelist_params.AD_CACHING_METRO_CODE}")
    os = "iOS" if "iOS" in plc_desc else "Android"

    if appnext_price:
        ext_for_ua_request = [ExtInfo4UARequest("381623", "380432", "0", "null", appnext_price, {})]
    else:
        ext_for_ua_request = None
    mutt_response, ua_response = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        im_plid=im_plid,
        as_plid=as_plid,
        os=os,
        adid=adid,
        has_dynamic_mediation=True,
        https=True,
        ext_for_ua_request=ext_for_ua_request,
        mutt_payload={"a-bid": a_bid},
    )
    assert len(mutt_response) == 3, "there should be 1 ump ad, and 2 dynamic ads"
    assert mutt_response.ad_sets[0].isAuctionClosed == False
    assert is_adv_price_as_expected([float(ad.macros["${advPrice}"]) for ad in ua_response.ads], expected_prices)


@pytest.mark.flaky(reruns=2)
@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, a_bid, expected_ads_from_mutt, expected_prices",
    [
        # bids price: $4, $5, $7, $10, $14
        ("Android - Winning UMP + AdColony + AppNext + RTB Vast", 381001, 1539906510215, micro(20), 6, [20.0]),
        # bids price: $5, $7, $10, $14, $20
        (
            "Android - Losing UMP + AdColony + AppNext + RTB Vast", 381004, 1539906510215, micro(15), 6,
            [17.554, 8.56, 10.01, 7.905] #[17.733, 8.56, 10.19, 8.084]
        ),
        # bids price: $4, $5, $7, $10, $14
        ("iOS - Winning UMP + AdColony + Vungle + RTB Vast", 381000, 1535339526186, micro(20), 6, [20.0]),
        # bids price: $5, $7, $10, $14, $20
        (
            "iOS - Losing UMP + AdColony + Vungle + RTB Vast", 381003, 1535339526186, micro(15), 6,
            [17.554, 8.56, 10.01, 8.782] #[17.733, 8.56, 10.19, 8.96]
        ),
    ]
)
def test_ua_response_for_as_ump(
    cache_client: pytest.fixture,
    plc_desc: str,
    as_plid: int,
    im_plid: int,
    a_bid: int,
    expected_ads_from_mutt: int,
    expected_prices: list,
):
    """
    all the as plcs have rev_share=0.57, just as UMP's default rev_share
    """
    # adid = str(uuid.uuid4())
    adid = "665438aa-1b22-46df-9f2d-07be100bd87c"
    print(f"cache_key: {as_plid}_{adid}_{whitelist_params.AD_CACHING_METRO_CODE}")
    os = "iOS" if "iOS" in plc_desc else "Android"
    cache_client.delete(f"{as_plid}_{adid}_{whitelist_params.AD_CACHING_METRO_CODE}")

    mutt_response, ua_response = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        as_plid=as_plid,
        im_plid=im_plid,
        os=os,
        adid=adid,
        has_dynamic_mediation=True,
        https=True,
        mutt_payload={"a-bid": a_bid},
    )

    assert len(mutt_response) == expected_ads_from_mutt, \
    "when ump is the winning ad, there should only be ump ad plus all dynamic ads from SDK to UA. \
    When UMP ad is the losing bid, there should be up to 4 AS ads plus all dynamic ads"

    assert mutt_response.ad_sets[0].isAuctionClosed == False
    assert is_adv_price_as_expected([float(ad.macros["${advPrice}"]) for ad in ua_response.ads], expected_prices)
