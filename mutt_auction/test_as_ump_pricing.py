from mutt import client
from mutt.common import AdType, is_attr_none
import pytest
import uuid
import mutt.request.request_mutt as mrp
from functools import reduce
from mutt.config import environment as mutt_env
from aerserv.config.aerserv import environment as as_env
from aerserv import whitelist_params
from mutt.request.request_mutt import micro


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, os, ad_type, a_bid, expected_pub_price",
    [
        # below bid price by default: $10, plc_price: $7, as_revShare = 0.6, ump_revShare = 0.57
        # ump pub price = max(plc_price, ump_bidprice * ump_revshare)
        (
            "Banner - Winning UMP - UMP pub price(6.84) lower than plc price(7.0)", 380992, 1537235559083, "iOS",
            AdType.BANNER.value, micro(12), 7.0
        ),
        (
            "Banner - Winning UMP - UMP pub price(7.41) above plc price(7.0)", 380992, 1537235559083, "iOS",
            AdType.BANNER.value, micro(13), 7.41
        ),
        (
            "Banner - Winning UMP - UMP pub price(6.84) lower than plc price(7.0)", 380992, 1539853098877, "Android",
            AdType.BANNER.value, micro(12), 7.0
        ),
        (
            "Banner - Winning UMP - UMP pub price(7.41) above plc price(7.0)", 380992, 1539853098877, "Android",
            AdType.BANNER.value, micro(13), 7.41
        ),
        # below bid price by default: $10, plc_price: NULL, as_revShare = 0.6, ump_revShare = 0.57
        # ump pub price = ump_bidprice * ump_revshare
        (
            "Interstitial - Winning UMP - No Plc Price ", 380991, 1535339526186, "iOS", AdType.INTERSTITIAL.value,
            micro(12), 6.84
        ),
        (
            "Interstitial - Winning UMP - No Plc Price", 380991, 1539906510215, "Android", AdType.INTERSTITIAL.value,
            micro(13), 7.41
        ),
        # # below bid price by default: $10, plc_price: $5, as_revShare = NULL, ump_revShare = 0.57
        # ump pub price = plc_price
        (
            "Interstitial - Winning UMP - No Rev Share ", 381005, 1535339526186, "iOS", AdType.INTERSTITIAL.value,
            micro(20), 5.0
        ),
        (
            "Interstitial - Winning UMP - No Rev Share ", 381005, 1539906510215, "Android", AdType.INTERSTITIAL.value,
            micro(8.5), 5.0
        ),
    ]
)
def test_close_auction_pricing(plc_desc, as_plid, im_plid, os, ad_type, a_bid, expected_pub_price):
    """
    when there is a plc_price from AS, ump pub price = max(ump_bid * rev_share, plc_price)
    where there is no plc_price from AS, ump pub price = ump_bid * rev_share
    """
    adid = str(uuid.uuid4())
    if as_plid == 381005:
        adid = "test_same_2_bids"  # two bids both at $4.0

    params = {"env": _ENV, "adtype": ad_type, "as_plid": as_plid, "im_plid": im_plid, "os": os, "adid": adid}
    mutt_response, _ = client.request(mutt_payload={"a-bid": a_bid}, **params)
    assert len(mutt_response) == (2 if as_plid == 381005 else 1), \
    "UMP ad should win, and only ump ad should be in the response"
    assert abs(mutt_response.ad_sets[0].ads[0].get("transaction").get("buyerPrice") - expected_pub_price) <= 0.01
