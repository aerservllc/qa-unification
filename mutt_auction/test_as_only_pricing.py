from mutt import client
from mutt.common import AdType, is_attr_none
import pytest
import uuid
import mutt.request.request_mutt as mrp
from functools import reduce
from mutt.config import environment as mutt_env
from aerserv.config.aerserv import environment as as_env
from aerserv import whitelist_params
"""
All PLCs are from this sheet:
https://docs.google.com/spreadsheets/d/1SoQVHA49jqT5LirepKWXW5i4ig9P4hpmLR5sneWIZnw/edit#gid=127647400
"""


def is_price_list_as_expected(buyer_prices: list, expected_pub_prices: list):
    if len(buyer_prices) != len(expected_pub_prices):
        return False
    print([abs(a - b) <= 0.01 for a, b in zip(buyer_prices, expected_pub_prices)])
    return all([abs(a - b) <= 0.01 for a, b in zip(buyer_prices, expected_pub_prices)])


@pytest.mark.parametrize(
    "plc_desc, as_plid, os, ad_type, expected_ad_number, expected_pub_prices",
    [
        (
            "only mediation, different ad source priorities", 380959, "Android", AdType.INTERSTITIAL.value, 3,
            [3.0, 4.0, 5.0]
        ),
        ("only mediation, same ad source priorities", 380961, "Android", AdType.INTERSTITIAL.value, 4, [4, 3, 6, 5]),
        (
            "Only Primary AerMarket Ad Source, with 2nd price ad lines", 380960, "Android", AdType.INTERSTITIAL.value,
            3, [2.408, 4.008, 4.808]
        ),
        (
            "Only Primary AerMarket Ad Source, with 2nd price ad lines", 380962, "Android", AdType.INTERSTITIAL.value,
            3, [3.508, 4.008, 4.808]
        ),
        (
            "Only Primary AerMarket Ad Source, with 2nd price ad lines", 380963, "Android", AdType.INTERSTITIAL.value,
            3, [4.808, 4.008, 2.408]
        ),
        (
            "Only Primary AerMarket Ad Source, with 2nd price ad lines", 380964, "Android", AdType.INTERSTITIAL.value,
            3, [4.808, 4.008, 3.508]
        ),
        # below plc has Secondary AerMarket CPM @ $0.0
        (
            "Only Secondary AerMarket Ad Source, with 2nd price ad lines", 380965, "Android", AdType.INTERSTITIAL.value,
            3, [0, 0, 0]
        ),
        # below plc has Secondary AerMarket CPM @ $3.5
        (
            "Only Secondary AerMarket Ad Source, with 2nd price ad lines", 380966, "Android", AdType.INTERSTITIAL.value,
            3, [3.5, 3.5, 3.5]
        ),
        # below plc has Secondary AerMarket CPM @ $0.0
        (
            "Only Secondary AerMarket Ad Source, with 2nd price ad lines", 380967, "Android", AdType.INTERSTITIAL.value,
            3, [0, 0, 0]
        ),
        # below plc has Secondary AerMarket CPM @ $3.5
        (
            "Only Secondary AerMarket Ad Source, with 2nd price ad lines", 380968, "Android", AdType.INTERSTITIAL.value,
            3, [3.5, 3.5, 3.5]
        ),
        (
            "Only Primary AerMarket Ad Source, with 1st price ad lines", 380969, "Android", AdType.INTERSTITIAL.value,
            3, [4, 4.8, 5.6]
        ),
        (
            "Only Primary AerMarket Ad Source, with 1st price ad lines", 380970, "Android", AdType.INTERSTITIAL.value,
            3, [4, 4.8, 5.6]
        ),
        (
            "Only Primary AerMarket Ad Source, with 1st price ad lines",
            380971,
            "Android",
            AdType.INTERSTITIAL.value,
            3,
            [5.6, 4.8, 4.0]  # but only got 2 prices. updated ad source json file
        ),
        (
            "Only Primary AerMarket Ad Source, with 1st price ad lines",
            380972,
            "Android",
            AdType.INTERSTITIAL.value,
            3,
            [5.6, 4.8, 4.0]  # but only got 2 prices. updated ad source json file
        ),
        # below plc has Secondary AerMarket CPM @ $0.0
        (
            "Only Secondary AerMarket Ad Source, with 1st price ad lines",
            380973,
            "Android",
            AdType.INTERSTITIAL.value,
            3,
            [0, 0, 0]  # but got [0,0,0]
        ),
        # below plc has Secondary AerMarket CPM @ $3.5
        (
            "Only Secondary AerMarket Ad Source, with 1st price ad lines",
            380974,
            "Android",
            AdType.INTERSTITIAL.value,
            3,
            [3.5, 3.5, 3.5]  # but got [0,0,0]
        ),
        # below plc has Secondary AerMarket CPM @ $0.0
        (
            "Only Secondary AerMarket Ad Source, with 1st price ad lines",
            380975,
            "Android",
            AdType.INTERSTITIAL.value,
            3,
            [0, 0, 0]  # but only got 2 prices. updated ad source json file
        ),
        # below plc has Secondary AerMarket CPM @ $3.5
        (
            "Only Secondary AerMarket Ad Source, with 1st price ad lines",
            380976,
            "Android",
            AdType.INTERSTITIAL.value,
            3,
            [3.5, 3.5, 3.5]  # but only got 2 prices. updated ad source json file
        ),
        (
            "Only Primary AerMarket with 1st and 2nd price",
            380977,
            "Android",
            AdType.INTERSTITIAL.value,
            4,
            [4.808, 4.8, 4.008, 4]  # but got 3 ads, updated ad source json file
        ),
        (
            "Only Primary AerMarket with 1st and 2nd price",
            380978,
            "Android",
            AdType.INTERSTITIAL.value,
            4,
            [4.808, 4.8, 4.008, 4]  # but got 3 ads, updated ad source json file
        ),
        # below plc has Secondary AerMarket CPM @ $0.0
        (
            "Only Secondary AerMarket with 1st and 2nd price",
            380979,
            "Android",
            AdType.INTERSTITIAL.value,
            4,
            [0, 0, 0, 0]  # but got 3 ads, updated ad source json file
        ),
        # below plc has Secondary AerMarket CPM @ $3.5
        (
            "Only Secondary AerMarket with 1st and 2nd price",
            380980,
            "Android",
            AdType.INTERSTITIAL.value,
            4,
            [3.5, 3.5, 3.5, 3.5]  # but got 3 ads, updated ad source json file
        ),
        # known issue: Known issue: when the final sorting face the problem where multiple buckets have the same highest price, because of the existence of line item priority, the final order is non-deterministic
        (
            "Mix of Mediation, Primary AerMarket, Secondary AerMarket wiht differnt ad source priorities",
            380981,
            "Android",
            AdType.INTERSTITIAL.value,
            4,
            [4.008, 4.008, 5.008, 4.0]  # but got [4.008, 4.008, 5.008, 4.0]
        ),
    ]
)
def test_close_auction_pricing(
    cache_client: pytest.fixture,
    plc_desc: str,
    as_plid: int,
    os: str,
    ad_type: str,
    expected_ad_number: int,
    expected_pub_prices: list,
):
    adid = str(uuid.uuid4())
    cache_key = f"{as_plid}_{adid}_{whitelist_params.AD_CACHING_METRO_CODE}"
    print(cache_key)
    cache_client.delete(cache_key)

    params = {
        "env": _ENV,
        "adtype": ad_type,
        "as_plid": as_plid,
        "os": os,
        "adid": adid,
        "has_dynamic_mediation": False,
    }
    mutt_response, _ = client.request(**params)
    ad_set = mutt_response.ad_sets[0]
    assert ad_set.isAuctionClosed == True
    assert len(mutt_response) == expected_ad_number

    buyer_prices = [ad.transaction.get("buyerPrice") for ad in ad_set.ads]
    assert is_price_list_as_expected(buyer_prices, expected_pub_prices)
