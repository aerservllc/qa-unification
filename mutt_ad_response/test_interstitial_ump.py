import re
import pytest
import mutt.request.request_mutt as mrp
from mutt import client
from mutt.common import AdType
from mutt.request.request_mutt import micro


#-------- close-auction=true --------
@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Interstitial UMP Android", 1539368680012, "Android"),
        ("Interstitial UMP Rewarded Android", 1538002975553, "Android"),
        ("Interstitial UMP Static Android", 1535989024460, "Android"),
        ("Interstitial UMP iOS", 1537156199489, "iOS"),
        ("Interstitial UMP Rewarded iOS", 1538155088824, "iOS"),
        ("Interstitial UMP Static iOS", 1538480801370, "iOS"),
    ]
)
def test_all_keys_available(plc_desc: str, plc: int, os: str):
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "im_plid": plc,
        "os": os,
    }
    mutt_response, _ = client.request(**params)
    expected_root_keys = set(["requestId", "adSets"])
    expected_ad_set_keys = set(["adSetId", "isAuctionClosed", "expiry", "ads"])
    expected_ad_keys = set(
        [
            "pubContent", "tracking", "markupType", "expiry", "impressionId", "canLoadBeforeShow", "metaInfo",
            "transaction", "trackingEvents", "creativeId", "asPlcId"
        ]
    )
    assert bool(mutt_response), "response shouldn't be empty"
    raw_response = mutt_response.raw_response()
    assert set(raw_response.keys()) == expected_root_keys
    adset = raw_response.get("adSets")[0]
    assert set(adset.keys()) == expected_ad_set_keys
    for ad in adset.get("ads"):
        assert set(ad.keys()) >= expected_ad_keys


@pytest.mark.parametrize(
    "plc_desc, plc, expected_is_auction_closed, os", [
        ("Interstitial UMP Android", 1539368680012, True, "Android"),
        ("Interstitial UMP Rewarded Android", 1538002975553, True, "Android"),
        ("Interstitial UMP Static Android", 1535989024460, True, "Android"),
        ("Interstitial UMP iOS", 1537156199489, True, "iOS"),
        ("Interstitial UMP Rewarded iOS", 1538155088824, True, "iOS"),
        ("Interstitial UMP Static iOS", 1538480801370, True, "iOS"),
    ]
)
def test_isAuctionClosed(plc_desc: str, plc: int, expected_is_auction_closed: bool, os: str):
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "im_plid": plc,
        "os": os,
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    is_auction_closed = mutt_response.ad_sets[0].get("isAuctionClosed")
    assert is_auction_closed == expected_is_auction_closed, \
        f"{plc_desc} -- expected {expected_is_auction_closed}, but got {is_auction_closed} instead"


@pytest.mark.parametrize(
    "plc_desc, plc, expected_tracking, os", [
        ("Interstitial UMP Android", 1539368680012, "unknown", "Android"),
        ("Interstitial UMP Rewarded Android", 1538002975553, "unknown", "Android"),
        ("Interstitial UMP Static Android", 1535989024460, "unknown", "Android"),
        ("Interstitial UMP iOS", 1537156199489, "unknown", "iOS"),
        ("Interstitial UMP Rewarded iOS", 1538155088824, "unknown", "iOS"),
        ("Interstitial UMP Static iOS", 1538480801370, "unknown", "iOS"),
    ]
)
def test_tracking(plc_desc: str, plc: int, expected_tracking: str, os: str):
    """
    Tracking type:
    Web: when serving video 3.0. Else, unknown.
    Only UMP supports video 3.0 as of now. AS is currently being served as video 2.0
    """
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "im_plid": plc,
        "os": os,
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    tracking = mutt_response.ad_sets[0].get("ads")[0].get("tracking")
    assert tracking == expected_tracking, \
        f"{plc_desc} -- expected {expected_tracking}, but got {tracking} instead"


@pytest.mark.parametrize(
    "plc_desc, plc, expected_markup_type, os", [
        ("Interstitial UMP Android", 1539368680012, ["html", "inmobiJson"], "Android"),
        ("Interstitial UMP Rewarded Android", 1538002975553, ["html", "inmobiJson"], "Android"),
        ("Interstitial UMP Static Android", 1535989024460, ["html"], "Android"),
        ("Interstitial UMP iOS", 1537156199489, ["html", "inmobiJson"], "iOS"),
        ("Interstitial UMP Rewarded iOS", 1538155088824, ["html", "inmobiJson"], "iOS"),
        ("Interstitial UMP Static iOS", 1538480801370, ["html"], "iOS"),
    ]
)
def test_markupType(plc_desc: str, plc: int, expected_markup_type: list, os: str):
    """
    The possible values that Mutt sends today are: html, inmobiJson and mediationJson,
    """
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "im_plid": plc,
        "os": os,
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    markup_type = mutt_response.ad_sets[0].get("ads")[0].get("markupType")
    assert markup_type in expected_markup_type, \
        f"{plc_desc} -- expected {expected_markup_type}, but got {markup_type} instead"


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Interstitial UMP Android", 1539368680012, "Android"),
        ("Interstitial UMP Rewarded Android", 1538002975553, "Android"),
        ("Interstitial UMP Static Android", 1535989024460, "Android"),
        ("Interstitial UMP iOS", 1537156199489, "iOS"),
        ("Interstitial UMP Rewarded iOS", 1538155088824, "iOS"),
        ("Interstitial UMP Static iOS", 1538480801370, "iOS"),
    ]
)
def test_expiry(plc_desc: str, plc: int, os: str):
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "im_plid": plc,
        "os": os,
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    expiry = mutt_response.ad_sets[0].get("ads")[0].get("expiry")
    assert expiry == 9000, "default value of expiry should be 9000"


@pytest.mark.parametrize(
    "plc_desc, plc", [
        ("Interstitial UMP iOS", 1537156199489),
        ("Interstitial UMP Rewarded iOS", 1538155088824),
        ("Interstitial UMP Static iOS", 1538480801370),
    ]
)
def test_safeArea(plc_desc: str, plc: int):
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "im_plid": plc,
        "os": "iOS",
        mrp.IOS_DEVICE_MACHINE_HARDWARE: "iPhone10,3",
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    safe_area = mutt_response.ad_sets[0].get("ads")[0].get("safeArea")
    expected_safe_area_keys = ["assetStyle"]
    expected_asset_style_keys = ["nonSafeAreaBackgroundColor", "geometry"]
    assert set(safe_area.keys()) ==set(expected_safe_area_keys)
    assert set(safe_area.get("assetStyle").keys()) ==set(expected_asset_style_keys)


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Interstitial UMP Android", 1539368680012, "Android"),
        ("Interstitial UMP Rewarded Android", 1538002975553, "Android"),
        ("Interstitial UMP Static Android", 1535989024460, "Android"),
        ("Interstitial UMP iOS", 1537156199489, "iOS"),
        ("Interstitial UMP Rewarded iOS", 1538155088824, "iOS"),
        ("Interstitial UMP Static iOS", 1538480801370, "iOS"),
    ]
)
def test_creativeId(plc_desc: str, plc: int, os: str):
    """
    Encrypted creative Id of the ad. Passed on to the publisher.
    This will currently exist only for IM ads.
    """
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "im_plid": plc,
        "os": os,
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    creative_id = mutt_response.ad_sets[0].get("ads")[0].get("creativeId")
    assert creative_id, "creativeId should exist for UMP ads"


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Interstitial UMP Android", 1539368680012, "Android"),
        ("Interstitial UMP Rewarded Android", 1538002975553, "Android"),
        ("Interstitial UMP Static Android", 1535989024460, "Android"),
        ("Interstitial UMP iOS", 1537156199489, "iOS"),
        ("Interstitial UMP Rewarded iOS", 1538155088824, "iOS"),
        ("Interstitial UMP Static iOS", 1538480801370, "iOS"),
    ]
)
def test_canLoadBeforeShow(plc_desc: str, plc: int, os: str):
    """
    AerServ's Ad responses cannot be loaded in the webview before the show is invoked. But UMP's ads can. This flag determines if the ad response can be loaded in the webview before the show is invoked or not.
    UMP ad: true
    AerMarket ad: true for markupType != html, false otherwise
    Mediation ad: true for native and interstitial. false for banner
    """
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "im_plid": plc,
        "os": os,
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    can_load_before_show = mutt_response.ad_sets[0].get("ads")[0].get("canLoadBeforeShow")
    assert can_load_before_show == True, "canLoadBeforeShow should be True for UMP ads"


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Interstitial UMP Android", 1539368680012, "Android"),
        ("Interstitial UMP Rewarded Android", 1538002975553, "Android"),
        ("Interstitial UMP Static Android", 1535989024460, "Android"),
        ("Interstitial UMP iOS", 1537156199489, "iOS"),
        ("Interstitial UMP Rewarded iOS", 1538155088824, "iOS"),
        ("Interstitial UMP Static iOS", 1538480801370, "iOS"),
    ]
)
def test_metaInfo(plc_desc: str, plc: int, os: str):
    """
    omidEnabled will be true if OMSDK needs to be initialized by our SDK (dictated by UMP demand).
    customReferenceData and isolateVerificationScripts are today sent as empty string and false,
    respectively from server side. These are also required by SDK for OMSDK init.
    Only supported for UMP. For AS, this will have "omidEnabled: false".
    Structure:
    {
    "omidEnabled": "true/false",
    "customReferenceData": "some-string",
    "isolateVerificationScripts": "true/false",
    "macros": {
        "key": "value",
        "key2": "value2"
    }
    """
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "im_plid": plc,
        "os": os,
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    expected_metainfo_keys = set(["creativeType", "omsdkInfo"])
    meta_info = mutt_response.ad_sets[0].get("ads")[0].get("metaInfo")
    assert set(meta_info.keys()) == expected_metainfo_keys
    assert meta_info.get("creativeType") in ["unknown", "video", "nonvideo"]
    assert meta_info.get("omsdkInfo").get("omidEnabled") == False
    assert meta_info.get("omsdkInfo").get("customReferenceData") == ""
    assert meta_info.get("omsdkInfo").get("isolateVerificationScripts") == False


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Interstitial UMP Android", 1539368680012, "Android"),
        ("Interstitial UMP Rewarded Android", 1538002975553, "Android"),
        ("Interstitial UMP Static Android", 1535989024460, "Android"),
        ("Interstitial UMP iOS", 1537156199489, "iOS"),
        ("Interstitial UMP Rewarded iOS", 1538155088824, "iOS"),
        ("Interstitial UMP Static iOS", 1538480801370, "iOS"),
    ]
)
def test_transaction(plc_desc: str, plc: int, os: str):
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "im_plid": plc,
        "os": os,
        "mutt_payload": {
            "a-bid": micro(20)
        }
    }
    mutt_response, _ = client.request(**params)
    expected_transaction_keys = set(["adSourceName", "buyerName", "buyerPrice", "ctxHash"])
    assert bool(mutt_response), "response shouldn't be empty"
    transaction = mutt_response.ad_sets[0].get("ads")[0].get("transaction")
    assert set(transaction.keys()) == expected_transaction_keys, \
        f"{plc_desc} -- keys mismatch in transactions"
    ad_source_name = transaction.get("adSourceName")
    buyer_name = transaction.get("buyerName")
    buyer_price = transaction.get("buyerPrice")
    assert ad_source_name == "AerMarket AdSource", \
        f"{plc_desc} -- expected 'AerMarket Adsource', but got {ad_source_name} instead"
    assert buyer_name == "InMobi", \
        f"{plc_desc} -- expected 'InMobi', but got {buyer_name} instead"
    assert round(buyer_price, 1) == 11.4, \
        f"{plc_desc} -- expected 11.4, ump ads have a default rev_share=0.57"


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Interstitial UMP Android", 1539368680012, "Android"),
        ("Interstitial UMP Rewarded Android", 1538002975553, "Android"),
        ("Interstitial UMP Static Android", 1535989024460, "Android"),
        ("Interstitial UMP iOS", 1537156199489, "iOS"),
        ("Interstitial UMP Rewarded iOS", 1538155088824, "iOS"),
        ("Interstitial UMP Static iOS", 1538480801370, "iOS"),
    ]
)
def test_tracking_events(plc_desc: str, plc: int, os: str):
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "im_plid": plc,
        "os": os,
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    tracking_events = mutt_response.ad_sets[0].get("ads")[0].get("trackingEvents")
    assert tracking_events == []


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Interstitial UMP Android", 1539368680012, "Android"),
        ("Interstitial UMP Rewarded Android", 1538002975553, "Android"),
        ("Interstitial UMP Static Android", 1535989024460, "Android"),
        ("Interstitial UMP iOS", 1537156199489, "iOS"),
        ("Interstitial UMP Rewarded iOS", 1538155088824, "iOS"),
        ("Interstitial UMP Static iOS", 1538480801370, "iOS"),
    ]
)
def test_asplcid(plc_desc: str, plc: int, os: str):
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "im_plid": plc,
        "os": os,
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    asplcid = mutt_response.ad_sets[0].get("ads")[0].get("asPlcId")
    assert asplcid == 0, \
        f"UMP placement without a mapping AS plc should have asplcid=0"


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Interstitial UMP Rewarded Android", 1538002975553, "Android"),
        ("Interstitial UMP Rewarded iOS", 1538155088824, "iOS"),
    ]
)
def test_rewards(plc_desc: str, plc: int, os: str):
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "im_plid": plc,
        "os": os,
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    markup_type = mutt_response.ad_sets[0].get("ads")[0].get("markupType")
    if markup_type == "inmobiJson":
        pubContent = mutt_response.ad_sets[0].get("ads")[0].get("pubContent")
        rewards_1 = pubContent.get("rewards")
        rewards_2 = list(
            filter(
                None, [assetValue.get("rewards") for assetValue in pubContent.get("rootContainer").get("assetValue")]
            )
        )[0]
        expected_rewards = {"default": "default"}
        assert rewards_1 == expected_rewards
        assert rewards_2 == expected_rewards


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Interstitial UMP Android", 1539368680012, "Android"),
        ("Interstitial UMP Rewarded Android", 1538002975553, "Android"),
        ("Interstitial UMP Static Android", 1535989024460, "Android"),
        ("Interstitial UMP iOS", 1537156199489, "iOS"),
        ("Interstitial UMP Rewarded iOS", 1538155088824, "iOS"),
        ("Interstitial UMP Static iOS", 1538480801370, "iOS"),
    ]
)
def test_applyBitmap(plc_desc: str, plc: int, os: str):
    """
    Always False for UMP or video ads.
    For AS banner ads, 'applyBitmap'=True only when 'canFailover'=True
    """
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "im_plid": plc,
        "os": os,
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    apply_bitmap = mutt_response.ad_sets[0].get("ads")[0].get("applyBitmap")
    markup_type = mutt_response.ad_sets[0].get("ads")[0].get("markupType")
    if markup_type == "html":
        assert apply_bitmap == False, \
            f"{plc_desc} -- expected False, but got {apply_bitmap} instead"
    else:
        assert apply_bitmap is None, \
            f"{plc_desc} -- expected None, but got {apply_bitmap} instead"


#-------- close-auction=false --------


@pytest.mark.parametrize(
    "plc_desc, os, as_plid, im_plid, a_bid", [
        ("Android - Winning UMP + AdColony + AppNext", "Android", 380999, 1539906510215, micro(20)),
        ("iOS - Losing UMP + AdColony + Vungle", "iOS", 380998, 1535339526186, micro(3)),
    ]
)
def test_non_close_auction_response(
    plc_desc: str,
    os: str,
    as_plid: int,
    im_plid: int,
    a_bid: int,
):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        im_plid=im_plid,
        as_plid=as_plid,
        os=os,
        has_dynamic_mediation=True,
        mutt_payload={"a-bid": a_bid},
    )

    assert len(mutt_response) == 3, "Winning UMP will be in the ad set"
    assert len(mutt_response.ad_sets) == 1, "there should be only 1 ad set"
    ad_set = mutt_response.ad_sets[0]
    assert set(ad_set.keys()) == {"adSetId", "expiry", "isAuctionClosed", "adSetAuctionMeta", "ads"}
    assert ad_set.get("adSetAuctionMeta") is not None
    for ad in mutt_response.ad_sets[0].ads:
        common_keys = {
            "pubContent",
            "tracking",
            "markupType",
            "expiry",
            "impressionId",
            "canLoadBeforeShow",
            "metaInfo",
            "transaction",
            "trackingEvents",
            "baseEventUrl",
            "asPlcId",
            "adAuctionMeta",
        }
        markup_type = ad.get("markupType")
        if markup_type == "html":
            expected_keys = common_keys.union({"applyBitmap", "creativeId"})
        elif markup_type == "mediationJson":
            expected_keys = common_keys.union({"viewability"})
        elif markup_type == "inmobiJson":
            expected_keys = common_keys.union({"creativeId"})
        assert set(ad.keys()) == expected_keys
        assert ad.get("adAuctionMeta") is not None
        transaction = ad.get("transaction")
        assert set(transaction.keys()) >= {"buyerName", "adSourceName"}
        assert "ctxHash" not in transaction and "buyerPrice" not in transaction
