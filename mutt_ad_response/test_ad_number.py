from mutt import client
from mutt.common import AdType, is_attr_none
import pytest
import uuid
import mutt.request.request_mutt as mrp
from functools import reduce
from mutt.config import environment as mutt_env
from aerserv.config.aerserv import environment as as_env
from aerserv.adserver.ad_cache import AdCache
from aerserv import whitelist_params
"""
ticket: https://aerserv.atlassian.net/browse/DEV-5006

Vast placements:
    - return up to 4 ads regardless of close-auction

Banner placements
    - For close-auction, Mutt would truncate the final list of ads till the point it finds one that has
      "canFailover = False", or the max number of ads to be sent to SDK is reached (currently set as 4).
      UMP ad will only be sent back to SDK when it is the top ad.
    - For non close-auction, it will return up to 4 ads, regardless of 'canFailover'

"""


@pytest.fixture(scope="module", autouse=True)
def cache_client(request):
    env = mutt_env.from_string(request.config.getoption("--env"))
    as_env = mutt_env.get(env, "AdServer", "as_endpoint")
    return AdCache(as_env)


@pytest.mark.parametrize(
    "plc_desc, plc, adid, expected_ad_number",
    [
        # close-auction
        ("3 Vast Ads from S2S", 380959, None, 3),
        ("9 Vast Ads from RTB and S2S", 380981, None, 4),  # upt to 4
        ("2 Custom JS Vast Ads", 380785, None, 2),
        ("3 Banner Ads from S2S", 380142, None, 1),
        ("Multiple RTB Interstital Static Bids", 380156, "test_5_static_bids_with_price_at_4_5_6_7_8", 1),
        ("2 Non-Dynamic SDK Banner Ads", 380024, None, 2),
        ("2 Non-Dynamic SDK Interstital Vast Ads", 380013, None, 2),
        ("RTB Interstital Static Bid winning + Custom JS", 380990, "test_5_static_bids_with_price_at_4_5_6_7_8", 1),

        # non-close-auction
        (
            "non-close-auction: Multiple RTB Insterstial Static Bids + Dynamic SDK Ad", 380988,
            "test_5_static_bids_with_price_at_4_5_6_7_8", 2
        ),
        (
            "non-close-auction: iOS - Multiple RTB Interstital Vast Bids + 2 Dynamic SDK Ads", 380989,
            "test_5_vast_bids_at_4_5_7_10_14", 6
        ),
        ("non-close-auction: 1 Non-Dynamic Interstital Vast SDK + 2 Dynamic Interstitial Vast SDK", 380744, None, 3),
    ]
)
def test_ad_number(cache_client: pytest.fixture, plc_desc: str, plc: int, adid: str or None, expected_ad_number: int):
    """
    How to classify ads:
    - Banner v.s. Interstitial
    - Within Interstital, static v.s. video(or vast)

    SDK ads could be Banner or Interstital
    """
    adid = (adid or str(uuid.uuid4()))
    cache_client.delete(f"{plc}_{adid}_{whitelist_params.AD_CACHING_METRO_CODE}")
    if "Vast" in plc_desc or "Interstital" in plc_desc:
        ad_type = AdType.INTERSTITIAL.value
    else:
        ad_type = AdType.BANNER.value
    has_dynamic_mediation = True if "non-close-auction" in plc_desc else False
    os = "iOS" if "iOS" in plc_desc else "Android"

    mutt_response, ua_response = client.request(
        env=_ENV,
        adtype=ad_type,
        as_plid=plc,
        os=os,
        adid=adid,
        has_dynamic_mediation=has_dynamic_mediation,
        https=True,
    )
    # print(mutt_response)
    adsets = mutt_response.ad_sets
    assert len(adsets) == 1, "there should be 1 adSet"
    assert len(adsets[0].ads) == expected_ad_number, \
               f"expecting {expected_ad_number} ads but got {len(adsets[0].ads)} ads"
    # print(adsets[0].__dict__)

    ad_set_auction_meta = adsets[0].adSetAuctionMeta
    ad_auction_meta_list = [int(bool(ad.adAuctionMeta)) for ad in adsets[0].ads]
    number_of_ad_auction_meta_info = reduce(lambda x, y: x + y, ad_auction_meta_list, 0)
    if has_dynamic_mediation:
        assert adsets[0].isAuctionClosed == False, "isAuctionClosed should be false if there is dynamic sdk"
        assert ad_set_auction_meta is not None and number_of_ad_auction_meta_info == expected_ad_number, \
        "each ad should have an adAuctionMeta object"
    else:
        assert adsets[0].isAuctionClosed == True, "isAuctionClosed should be True if there is no dynamic sdk"
        assert ad_set_auction_meta is None and number_of_ad_auction_meta_info == 0, \
        "all ads will have implicit info, no meta info should be present"
