import re, json


def check_AS_tracking_events_in_UMP_ads(pub_content: str, markup_type: str, creative_type: str):
    """
    markup_type can only be inmobijson or html
    """
    if markup_type.lower() == "inmobijson" or (markup_type.lower() == "html" and creative_type == "video"):
        expected_events = [2, 3, 4, 5, 6, 7, 18]
    elif markup_type.lower() == "html" and creative_type == "nonvideo":
        expected_events = [22]
    else:
        return
    for ev in expected_events:
        print(f"ev: {ev}")
        event_url_pattern = f".*ads.aerserv.com.*?ev={ev}.*?"
        event_url = re.search(event_url_pattern, pub_content).group(0)
        print(f"event_url: {event_url}")
        assert event_url is not None, f"should have a ev={ev} url in the pubContent"


def check_UMP_beacon_in_AS_ads(pub_content: str):
    beacon_url_pattern = "(https:\/\/et\.w\.inmobi\.com\/c\.asm\/.*?)"
    beacon_url = re.search(beacon_url_pattern, pub_content).group(1)
    assert beacon_url is not None, "should have a beacon url"


def check_pixelate_tracking_in_pubcontent(pub_content: str, markup_type: str):
    if markup_type.lower() not in ["inmobijson", "html"]:
        return
    pixelate_tracker_url_pattern = "(https:\/\/adrta\.com\/i\?.*?)"
    pixelate_tracker_url = re.search(pixelate_tracker_url_pattern, pub_content).group(1)
    assert pixelate_tracker_url is not None, "should have a pixelate tracker url"
