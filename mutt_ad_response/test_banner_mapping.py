import re
import pytest
import mutt.request.request_mutt as mrp
import verify_tracking_events as vte
from mutt import client
from mutt.common import AdType
from urllib.parse import parse_qs, urlparse
from collections import Counter
from mutt.request.request_mutt import micro

_A_BID_UMP_WIN = micro(20)
_A_BID_UMP_LOSS = micro(1)


#-------- close-auction=true --------
@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, os, a_bid", [
        ("Banner Mirror 320x50 Android - UMP WIN", 380992, 1539853098877, "Android", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 iOS - UMP WIN", 380992, 1537235559083, "iOS", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 Android - UMP LOSS", 380992, 1539853098877, "Android", _A_BID_UMP_LOSS),
        ("Banner Mirror 320x50 iOS - UMP LOSS", 380992, 1537235559083, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_all_keys_available(plc_desc: str, as_plid: int, im_plid: int, os: str, a_bid: int):
    mutt_response, _ = client.request(
        env=_ENV, adtype=AdType.BANNER.value, as_plid=as_plid, im_plid=im_plid, os=os, mutt_payload={"a-bid": a_bid}
    )
    expected_root_keys = set(["requestId", "adSets"])
    expected_ad_set_keys = set(["adSetId", "isAuctionClosed", "expiry", "ads"])
    expected_ad_keys = set(
        [
            "pubContent", "tracking", "markupType", "expiry", "impressionId", "canLoadBeforeShow", "metaInfo",
            "transaction", "trackingEvents", "baseEventUrl", "asPlcId", "applyBitmap"
        ]
    )
    assert bool(mutt_response), "response shouldn't be empty"
    raw_response = mutt_response.raw_response()
    assert set(raw_response.keys()) == expected_root_keys
    adset = raw_response.get("adSets")[0]
    assert set(adset.keys()) == expected_ad_set_keys
    for ad in adset.get("ads"):
        assert set(ad.keys()) >= expected_ad_keys


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, expected_is_auction_closed, os, a_bid", [
        ("Banner Mirror 320x50 Android - UMP WIN", 380992, 1539853098877, True, "Android", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 iOS - UMP WIN", 380992, 1537235559083, True, "iOS", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 Android - UMP LOSS", 380992, 1539853098877, True, "Android", _A_BID_UMP_LOSS),
        ("Banner Mirror 320x50 iOS - UMP LOSS", 380992, 1537235559083, True, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_isAuctionClosed(
    plc_desc: str, as_plid: int, im_plid: int, expected_is_auction_closed: bool, os: str, a_bid: int
):
    mutt_response, _ = client.request(
        env=_ENV, adtype=AdType.BANNER.value, as_plid=as_plid, im_plid=im_plid, os=os, mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    is_auction_closed = mutt_response.ad_sets[0].get("isAuctionClosed")
    assert is_auction_closed == expected_is_auction_closed, \
        f"{plc_desc} -- expected {expected_is_auction_closed}, but got {is_auction_closed} instead"


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, os, a_bid", [
        ("Banner Mirror 320x50 Android - UMP WIN", 380992, 1539853098877, "Android", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 iOS - UMP WIN", 380992, 1537235559083, "iOS", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 Android - UMP LOSS", 380992, 1539853098877, "Android", _A_BID_UMP_LOSS),
        ("Banner Mirror 320x50 iOS - UMP LOSS", 380992, 1537235559083, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_tracking(plc_desc: str, as_plid: int, im_plid: int, os: str, a_bid: int):
    """
    Tracking type:
    Web: when serving video 3.0. Else, unknown.
    Only UMP supports video 3.0 as of now. AS is currently being served as video 2.0
    """
    mutt_response, _ = client.request(
        env=_ENV, adtype=AdType.BANNER.value, as_plid=as_plid, im_plid=im_plid, os=os, mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    tracking = mutt_response.ad_sets[0].get("ads")[0].get("tracking")
    assert tracking == "unknown", \
        f"{plc_desc} -- expected 'unknown', but got {tracking} instead"


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, expected_markup_type, os, a_bid", [
        ("Banner Mirror 320x50 Android - UMP WIN", 380992, 1539853098877, "html", "Android", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 iOS - UMP WIN", 380992, 1537235559083, "html", "iOS", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 Android - UMP LOSS", 380992, 1539853098877, "html", "Android", _A_BID_UMP_LOSS),
        ("Banner Mirror 320x50 iOS - UMP LOSS", 380992, 1537235559083, "html", "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_markupType(plc_desc: str, as_plid: int, im_plid: int, expected_markup_type: str, os: str, a_bid: int):
    """
    The possible values that Mutt sends today are: html, inmobiJson and mediationJson,
    """
    mutt_response, _ = client.request(
        env=_ENV, adtype=AdType.BANNER.value, as_plid=as_plid, im_plid=im_plid, os=os, mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    markup_type = mutt_response.ad_sets[0].get("ads")[0].get("markupType")
    assert markup_type == expected_markup_type, \
        f"{plc_desc} -- expected {expected_markup_type}, but got {markup_type} instead"


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, expected_expiry, os, a_bid", [
        ("Banner Mirror 320x50 Android - UMP WIN", 380992, 1539853098877, 9000, "Android", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 iOS - UMP WIN", 380992, 1537235559083, 9000, "iOS", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 Android - UMP LOSS", 380992, 1539853098877, 600, "Android", _A_BID_UMP_LOSS),
        ("Banner Mirror 320x50 iOS - UMP LOSS", 380992, 1537235559083, 600, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_expiry(plc_desc: str, as_plid: int, im_plid: int, expected_expiry: int, os: str, a_bid: int):
    mutt_response, _ = client.request(
        env=_ENV, adtype=AdType.BANNER.value, as_plid=as_plid, im_plid=im_plid, os=os, mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    expiry = mutt_response.ad_sets[0].get("ads")[0].get("expiry")
    assert expiry == expected_expiry, \
        f"{plc_desc} -- expected {expected_expiry}, but got {expiry} instead"


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, a_bid", [
        ("Banner Mirror 320x50 iOS - UMP WIN", 380992, 1537235559083, _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 iOS - UMP LOSS", 380992, 1537235559083, _A_BID_UMP_LOSS),
    ]
)
def test_safeArea(plc_desc: str, as_plid: int, im_plid: int, a_bid: int):
    params = {
        "env": _ENV,
        "adtype": AdType.BANNER.value,
        "as_plid": as_plid,
        "im_plid": im_plid,
        "os": "iOS",
        mrp.IOS_DEVICE_MACHINE_HARDWARE: "iPhone10,3",
        "a-bid": a_bid
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    safe_area = mutt_response.ad_sets[0].get("ads")[0].get("safeArea")
    assert safe_area is None, "safeArea shouldn't exist for banner ads"


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, has_creative_id, os, a_bid", [
        ("Banner Mirror 320x50 Android - UMP WIN", 380992, 1539853098877, True, "Android", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 iOS - UMP WIN", 380992, 1537235559083, True, "iOS", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 Android - UMP LOSS", 380992, 1539853098877, False, "Android", _A_BID_UMP_LOSS),
        ("Banner Mirror 320x50 iOS - UMP LOSS", 380992, 1537235559083, False, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_creativeId(plc_desc: str, as_plid: int, im_plid: int, has_creative_id: bool, os: str, a_bid: int):
    """
    Encrypted creative Id of the ad. Passed on to the publisher.
    This will currently exist only for IM ads.
    """
    mutt_response, _ = client.request(
        env=_ENV, adtype=AdType.BANNER.value, as_plid=as_plid, im_plid=im_plid, os=os, mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    creative_id = mutt_response.ad_sets[0].get("ads")[0].get("creativeId")
    assert bool(creative_id) == has_creative_id, "creativeId should exist for UMP ads but not for AS ads"


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, expected_can_load_before_show, os, a_bid", [
        ("Banner Mirror 320x50 Android - UMP WIN", 380992, 1539853098877, True, "Android", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 iOS - UMP WIN", 380992, 1537235559083, True, "iOS", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 Android - UMP LOSS", 380992, 1539853098877, False, "Android", _A_BID_UMP_LOSS),
        ("Banner Mirror 320x50 iOS - UMP LOSS", 380992, 1537235559083, False, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_canLoadBeforeShow(
    plc_desc: str, as_plid: int, im_plid: int, expected_can_load_before_show: bool, os: str, a_bid: int
):
    """
    AerServ's Ad responses cannot be loaded in the webview before the show is invoked. But UMP's ads can. This flag determines if the ad response can be loaded in the webview before the show is invoked or not.
    UMP ad: true
    AerMarket ad: true for markupType != html, false otherwise
    Mediation ad: true for native and interstitial. false for banner
    """
    mutt_response, _ = client.request(
        env=_ENV, adtype=AdType.BANNER.value, as_plid=as_plid, im_plid=im_plid, os=os, mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    can_load_before_show = mutt_response.ad_sets[0].get("ads")[0].get("canLoadBeforeShow")
    assert can_load_before_show == expected_can_load_before_show, \
        f"{plc_desc} -- expected {expected_can_load_before_show}, but got {can_load_before_show} instead"


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, expected_creative_type, os, a_bid", [
        ("Banner Mirror 320x50 Android - UMP WIN", 380992, 1539853098877, "nonvideo", "Android", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 iOS - UMP WIN", 380992, 1537235559083, "nonvideo", "iOS", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 Android - UMP LOSS", 380992, 1539853098877, "nonvideo", "Android", _A_BID_UMP_LOSS),
        ("Banner Mirror 320x50 iOS - UMP LOSS", 380992, 1537235559083, "nonvideo", "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_metaInfo(plc_desc: str, as_plid: int, im_plid: int, expected_creative_type: str, os: str, a_bid: int):
    """
    omidEnabled will be true if OMSDK needs to be initialized by our SDK (dictated by UMP demand).
    customReferenceData and isolateVerificationScripts are today sent as empty string and false,
    respectively from server side. These are also required by SDK for OMSDK init.
    Only supported for UMP. For AS, this will have "omidEnabled: false".
    Structure:
    {
    "omidEnabled": "true/false",
    "customReferenceData": "some-string",
    "isolateVerificationScripts": "true/false",
    "macros": {
        "key": "value",
        "key2": "value2"
    }
    """
    mutt_response, _ = client.request(
        env=_ENV, adtype=AdType.BANNER.value, as_plid=as_plid, im_plid=im_plid, os=os, mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    expected_metainfo_keys = set(["creativeType", "omsdkInfo"])
    meta_info = mutt_response.ad_sets[0].get("ads")[0].get("metaInfo")
    assert set(meta_info.keys()) == expected_metainfo_keys
    assert meta_info.get("creativeType") == expected_creative_type
    assert meta_info.get("omsdkInfo").get("omidEnabled") == False
    assert meta_info.get("omsdkInfo").get("customReferenceData") == ("" if "UMP WIN" in plc_desc else None)
    assert meta_info.get("omsdkInfo").get("isolateVerificationScripts") == False


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, os, a_bid", [
        ("Banner Mirror 320x50 Android - UMP WIN", 380992, 1539853098877, "Android", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 iOS - UMP WIN", 380992, 1537235559083, "iOS", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 Android - UMP LOSS", 380992, 1539853098877, "Android", _A_BID_UMP_LOSS),
        ("Banner Mirror 320x50 iOS - UMP LOSS", 380992, 1537235559083, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_transaction(plc_desc: str, as_plid: int, im_plid: int, os: str, a_bid: int):
    mutt_response, _ = client.request(
        env=_ENV, adtype=AdType.BANNER.value, as_plid=as_plid, im_plid=im_plid, os=os, mutt_payload={"a-bid": a_bid}
    )
    expected_transaction_keys = set(["adSourceName", "buyerName", "buyerPrice", "ctxHash"])
    assert bool(mutt_response), "response shouldn't be empty"
    transaction = mutt_response.ad_sets[0].get("ads")[0].get("transaction")
    if "UMP LOSS" in plc_desc:
        expected_transaction_keys.update(set(["dspId", "adomain"]))
    assert set(transaction.keys()) == expected_transaction_keys, \
        f"{plc_desc} -- keys mismatch in transactions"
    ad_source_name = transaction.get("adSourceName")
    buyer_name = transaction.get("buyerName")
    buyer_price = transaction.get("buyerPrice")
    if "UMP WIN" in plc_desc:
        assert ad_source_name == "AerMarket AdSource"
        assert buyer_name == "InMobi"
        assert round(buyer_price, 1) == 11.4
    else:
        dsp_id = transaction.get("dspId")
        adomain = transaction.get("adomain")
        assert ad_source_name == "AerMarket Test Page"
        assert buyer_name == "AerMarket"
        assert dsp_id == "DSPMock"
        assert adomain == "aerserv.com"
        assert buyer_price == 7.0


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, os, expected_tracking_ev_types, a_bid", [
        ("Banner Mirror 320x50 Android - UMP WIN", 380992, 1539853098877, "Android", [], _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 iOS - UMP WIN", 380992, 1537235559083, "iOS", [], _A_BID_UMP_WIN),
        (
            "Banner Mirror 320x50 Android - UMP LOSS", 380992, 1539853098877, "Android", ["banner_rendered"],
            _A_BID_UMP_LOSS
        ),
        ("Banner Mirror 320x50 iOS - UMP LOSS", 380992, 1537235559083, "iOS", ["banner_rendered"], _A_BID_UMP_LOSS),
    ]
)
def test_tracking_events(
    plc_desc: str, as_plid: int, im_plid: int, os: str, expected_tracking_ev_types: list, a_bid: int
):
    mutt_response, _ = client.request(
        env=_ENV, adtype=AdType.BANNER.value, as_plid=as_plid, im_plid=im_plid, os=os, mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    tracking_events = mutt_response.ad_sets[0].get("ads")[0].get("trackingEvents")
    if "UMP WIN" in plc_desc:
        assert tracking_events == []
    else:
        expected_tracking_events_keys = ["type", "trackingUrls"]
        expected_tracking_params = [
            "plc", "rid", "pubid", "appid", "gdpr", "gdpr_consent", "os", "adid", "sdkv", "iline", "asplcid", "asid",
            "campaignid", "pinned", "vc_user_id", "buyer", "hck", "icid", "xcid", "ntid", "ctxhash", "eu", "ev",
            "pchain", "rtbdspid", "rtbbidid", "rtbrid"
        ]
        assert len(tracking_events) == len(expected_tracking_ev_types)
        for tracking in tracking_events:
            assert set(tracking.keys()) ==set(expected_tracking_events_keys)
            assert tracking.get("type") in expected_tracking_ev_types
            tracking_params = parse_qs(urlparse(tracking.get("trackingUrls")[0]).query, True)
            assert all([len(value) == 1 for value in tracking_params.values()]), \
                "there shouldn't be any duplicate keys"
            assert set(tracking_params.keys()) <=set(expected_tracking_params)


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, os, a_bid", [
        ("Banner Mirror 320x50 Android - UMP WIN", 380992, 1539853098877, "Android", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 iOS - UMP WIN", 380992, 1537235559083, "iOS", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 Android - UMP LOSS", 380992, 1539853098877, "Android", _A_BID_UMP_LOSS),
        ("Banner Mirror 320x50 iOS - UMP LOSS", 380992, 1537235559083, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_base_event_url(plc_desc: str, as_plid: int, im_plid: int, os: str, a_bid: int):
    mutt_response, _ = client.request(
        env=_ENV, adtype=AdType.BANNER.value, as_plid=as_plid, im_plid=im_plid, os=os, mutt_payload={"a-bid": a_bid}
    )
    expected_parameters_in_base_event_url = [
        "plc", "rid", "pubid", "appid", "gdpr", "gdpr_consent", "os", "adid", "sdkv", "iline", "asplcid", "asid",
        "campaignid", "pinned", "vc_user_id", "buyer", "icid", "ntid", "ctxhash", "eu", "ev", "pchain"
    ]
    if "UMP LOSS" in plc_desc:
        expected_parameters_in_base_event_url.extend(["hck", "xcid"])
    assert len(mutt_response), "response shouldn't be empty"
    base_event_url = mutt_response.ad_sets[0].get("ads")[0].get("baseEventUrl")
    event_params = parse_qs(urlparse(base_event_url).query, True)
    assert Counter(event_params.keys()) == Counter(expected_parameters_in_base_event_url)
    for param in ["ctxhash", "eu", "ev"]:
        assert event_params[param] == ["${" + param + "}"]


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, expected_asplcid, os, a_bid", [
        ("Banner Mirror 320x50 Android - UMP WIN", 380992, 1539853098877, 381612, "Android", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 iOS - UMP WIN", 380992, 1537235559083, 381612, "iOS", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 Android - UMP LOSS", 380992, 1539853098877, 381612, "Android", _A_BID_UMP_LOSS),
        ("Banner Mirror 320x50 iOS - UMP LOSS", 380992, 1537235559083, 381612, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_asplcid(plc_desc: str, as_plid: int, im_plid: int, expected_asplcid: int, os: str, a_bid: int):
    mutt_response, _ = client.request(
        env=_ENV, adtype=AdType.BANNER.value, as_plid=as_plid, im_plid=im_plid, os=os, mutt_payload={"a-bid": a_bid}
    )
    assert len(mutt_response), "response shouldn't be empty"
    asplcid = mutt_response.ad_sets[0].get("ads")[0].get("asPlcId")
    assert asplcid == expected_asplcid, \
        f"{plc_desc} -- expected {expected_asplcid}, but got {asplcid} instead"


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, os, a_bid", [
        ("Banner Mirror 320x50 Android - UMP WIN", 380992, 1539853098877, "Android", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 iOS - UMP WIN", 380992, 1537235559083, "iOS", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 Android - UMP LOSE", 380992, 1539853098877, "Android", _A_BID_UMP_LOSS),
        ("Banner Mirror 320x50 iOS - UMP LOSE", 380992, 1537235559083, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_tracking_pixels_in_pubcontent(plc_desc: str, as_plid: int, im_plid: int, os: str, a_bid: int):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        as_plid=as_plid,
        im_plid=im_plid,
        os=os,
        mutt_payload={"a-bid": a_bid},
    )
    assert bool(mutt_response), "response shouldn't be empty"
    pub_content = str(mutt_response.ad_sets[0].get("ads")[0].get("pubContent"))
    if "UMP LOSE" in plc_desc:
        vte.check_UMP_beacon_in_AS_ads(pub_content)
        vte.check_pixelate_tracking_in_pubcontent(pub_content, "html")
    else:
        vte.check_AS_tracking_events_in_UMP_ads(pub_content, "html", "nonvideo")


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, os, a_bid", [
        ("Banner Mirror 320x50 Android - UMP WIN", 380992, 1539853098877, "Android", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 iOS - UMP WIN", 380992, 1537235559083, "iOS", _A_BID_UMP_WIN),
        ("Banner Mirror 320x50 Android - UMP LOSS", 380992, 1539853098877, "Android", _A_BID_UMP_LOSS),
        ("Banner Mirror 320x50 iOS - UMP LOSS", 380992, 1537235559083, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_applyBitmap(plc_desc: str, as_plid: int, im_plid: int, os: str, a_bid: int):
    """
    Always False for UMP or video ads.
    For AS banner ads, 'applyBitmap'=True only when 'canFailover'=True
    """
    mutt_response, _ = client.request(
        env=_ENV, adtype=AdType.BANNER.value, as_plid=as_plid, im_plid=im_plid, os=os, mutt_payload={"a-bid": a_bid}
    )
    assert len(mutt_response), "response shouldn't be empty"
    apply_bitmap = mutt_response.ad_sets[0].get("ads")[0].get("applyBitmap")
    assert apply_bitmap == False, \
        f"{plc_desc} -- expected False, but got {apply_bitmap} instead"
