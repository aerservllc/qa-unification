import pytest
import mutt.request.request_mutt as mrp
from mutt import client
from aerserv.config.aerserv import environment as as_env
from mutt.config import environment as mutt_env
from couchbase.bucket import Bucket
from mutt.common import AdType
from datetime import datetime
import time
from couchbase.cluster import Cluster, PasswordAuthenticator


@pytest.fixture(autouse=True)
def get_budget_bucket(request):
    '''Establish and close the connection to couchbase budget'''
    env = request.config.getoption("--env")
    aerserv_env = mutt_env.get(env, "AdServer", 'as_endpoint')

    couchbase_host = as_env.get(aerserv_env, "Couchbase", "ad_server_host")
    try:
        couchbase_username = as_env.get(aerserv_env, "Couchbase", "ad_server_username")
        couchbase_password = as_env.get(aerserv_env, "Couchbase", "ad_server_password")
    except:
        print("No couchbase username,password configuration found, skipping")
        couchbase_username = None
        couchbase_password = None

    cluster = Cluster(f"couchbase://{couchbase_host}")

    if couchbase_username and couchbase_password:
        auth = PasswordAuthenticator(couchbase_username, couchbase_password)
        cluster.authenticate(auth)

    bucket = cluster.open_bucket("budget")
    return bucket


def test_close_auction_logic(get_budget_bucket: pytest.fixture):
    """
    https://aerserv.atlassian.net/browse/DEV-4961
    Ad server will check dynamic sdk ad sources' budget.
    When a Mutt->AS request has 'has-dynamic-mediation'=True and the plc has a dynamic ad source,
    after the budget check, mutt should close auction if the dynamic ad source runs out of budget.

    Test Strategy:
    First 3 requests should return 2 ads (auction is not closed yet).
    On the 4th request, there should be 1 RTB ad (auction is closed this time for dynamic sdk).
    """
    julian_date = str(datetime.utcnow().year) + str(datetime.utcnow().timetuple().tm_yday)
    budget_key = f"asdr_380674_{julian_date}"
    get_budget_bucket.upsert(budget_key, 0)
    time.sleep(3)

    for i in range(3):
        mutt_response, _ = client.request(
            env=_ENV,
            adtype=AdType.INTERSTITIAL.value,
            as_plid=381002,
            os="iOS",
            has_dynamic_mediation=True,
        )
        assert bool(mutt_response), "response shouldn't be empty"
        ad_set = mutt_response.ad_sets[0]
        assert ad_set.get("isAuctionClosed") == False
        ads = ad_set.ads
        assert len(ads) == 2

    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        as_plid=381002,
        os="iOS",
        has_dynamic_mediation=True,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    ad_set = mutt_response.ad_sets[0]
    assert ad_set.get("isAuctionClosed") == True
    ads = ad_set.ads
    assert len(ads) == 1
