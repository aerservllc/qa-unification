import re
import pytest
import mutt.request.request_mutt as mrp
from mutt import client
from mutt.common import AdType
from mutt.request.request_mutt import micro


#-------- close-auction=true --------
@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Banner UMP 320x50 Android", 1538848570905, "Android"),
        ("Banner UMP 320x50 iOS", 1535295950858, "iOS"),
    ]
)
def test_all_keys_available(plc_desc: str, plc: int, os: str):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        im_plid=plc,
        os=os,
    )
    expected_root_keys = set(["requestId", "adSets"])
    expected_ad_set_keys = set(["adSetId", "isAuctionClosed", "expiry", "ads"])
    expected_ad_keys = set(
        [
            "pubContent", "tracking", "markupType", "expiry", "impressionId", "canLoadBeforeShow", "metaInfo",
            "transaction", "trackingEvents", "creativeId", "asPlcId", "applyBitmap"
        ]
    )
    assert bool(mutt_response), "response shouldn't be empty"
    raw_mutt_response = mutt_response.raw_response()
    assert set(raw_mutt_response.keys()) == expected_root_keys
    adset = raw_mutt_response.get("adSets")[0]
    assert set(adset.keys()) == expected_ad_set_keys
    for ad in adset.get("ads"):
        assert set(ad.keys()) >= expected_ad_keys


@pytest.mark.parametrize(
    "plc_desc, plc, expected_is_auction_closed, os", [
        ("Banner UMP 320x50 Android", 1538848570905, True, "Android"),
        ("Banner UMP 320x50 iOS", 1535295950858, True, "iOS"),
    ]
)
def test_isAuctionClosed(plc_desc: str, plc: int, expected_is_auction_closed: bool, os: str):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        im_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    is_auction_closed = mutt_response.ad_sets[0].get("isAuctionClosed")
    assert is_auction_closed == expected_is_auction_closed, \
        f"{plc_desc} -- expected {expected_is_auction_closed}, but got {is_auction_closed} instead"


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Banner UMP 320x50 Android", 1538848570905, "Android"),
        ("Banner UMP 320x50 iOS", 1535295950858, "iOS"),
    ]
)
def test_tracking(plc_desc: str, plc: int, os: str):
    """
    Tracking type:
    Web: when serving video 3.0. Else, unknown.
    Only UMP supports video 3.0 as of now. AS is currently being served as video 2.0
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        im_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    tracking = mutt_response.ad_sets[0].get("ads")[0].get("tracking")
    assert tracking == "unknown", \
        f"{plc_desc} -- expected 'unknown', but got {tracking} instead"


@pytest.mark.parametrize(
    "plc_desc, plc, expected_markup_type, os", [
        ("Banner UMP 320x50 Android", 1538848570905, "html", "Android"),
        ("Banner UMP 320x50 iOS", 1535295950858, "html", "iOS"),
    ]
)
def test_markupType(plc_desc: str, plc: int, expected_markup_type: str, os: str):
    """
    The possible values that Mutt sends today are: html, inmobiJson and mediationJson,
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        im_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    markup_type = mutt_response.ad_sets[0].get("ads")[0].get("markupType")
    assert markup_type == expected_markup_type, \
        f"{plc_desc} -- expected {expected_markup_type}, but got {markup_type} instead"


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Banner UMP 320x50 Android", 1538848570905, "Android"),
        ("Banner UMP 320x50 iOS", 1535295950858, "iOS"),
    ]
)
def test_expiry(plc_desc: str, plc: int, os: str):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        im_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    expiry = mutt_response.ad_sets[0].get("ads")[0].get("expiry")
    assert expiry == 9000, "value of expiry should be 9000"


@pytest.mark.parametrize("plc_desc, plc", [
    ("Banner UMP 320x50 iOS", 1535295950858),
])
def test_safeArea(plc_desc: str, plc: int):
    params = {
        "env": _ENV,
        "adtype": AdType.BANNER.value,
        "im_plid": plc,
        "os": "iOS",
        mrp.IOS_DEVICE_MACHINE_HARDWARE: "iPhone10,3",
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    safe_area = mutt_response.ad_sets[0].get("ads")[0].get("safeArea")
    assert safe_area is None, "safeArea shouldn't exist for banner ads"


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Banner UMP 320x50 Android", 1538848570905, "Android"),
        ("Banner UMP 320x50 iOS", 1535295950858, "iOS"),
    ]
)
def test_creativeId(plc_desc: str, plc: int, os: str):
    """
    Encrypted creative Id of the ad. Passed on to the publisher.
    This will currently exist only for IM ads.
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        im_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    creative_id = mutt_response.ad_sets[0].get("ads")[0].get("creativeId")
    assert creative_id, "creativeId should exist for UMP ads"


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Banner UMP 320x50 Android", 1538848570905, "Android"),
        ("Banner UMP 320x50 iOS", 1535295950858, "iOS"),
    ]
)
def test_canLoadBeforeShow(plc_desc: str, plc: int, os: str):
    """
    AerServ's Ad responses cannot be loaded in the webview before the show is invoked. But UMP's ads can. This flag determines if the ad response can be loaded in the webview before the show is invoked or not.
    UMP ad: true
    AerMarket ad: true for markupType != html, false otherwise
    Mediation ad: true for native and interstitial. false for banner
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        im_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    can_load_before_show = mutt_response.ad_sets[0].get("ads")[0].get("canLoadBeforeShow")
    assert can_load_before_show == True, "canLoadBeforeShow should be True for UMP ads"


@pytest.mark.parametrize(
    "plc_desc, plc, expected_creativeType, os", [
        ("Banner UMP 320x50 Android", 1538848570905, "nonvideo", "Android"),
        ("Banner UMP 320x50 iOS", 1535295950858, "nonvideo", "iOS"),
    ]
)
def test_metaInfo(plc_desc: str, plc: int, os: str, expected_creativeType: str):
    """
    omidEnabled will be true if OMSDK needs to be initialized by our SDK (dictated by UMP demand).
    customReferenceData and isolateVerificationScripts are today sent as empty string and false,
    respectively from server side. These are also required by SDK for OMSDK init.
    Only supported for UMP. For AS, this will have "omidEnabled: false".
    Structure:
    {
    "omidEnabled": "true/false",
    "customReferenceData": "some-string",
    "isolateVerificationScripts": "true/false",
    "macros": {
        "key": "value",
        "key2": "value2"
    }
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        im_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    expected_metainfo_keys = set(["creativeType", "omsdkInfo"])
    meta_info = mutt_response.ad_sets[0].get("ads")[0].get("metaInfo")
    assert set(meta_info.keys()) == expected_metainfo_keys
    assert meta_info.get("creativeType") == expected_creativeType
    assert meta_info.get("omsdkInfo").get("omidEnabled") == False
    assert meta_info.get("omsdkInfo").get("customReferenceData") == ""
    assert meta_info.get("omsdkInfo").get("isolateVerificationScripts") == False


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Banner UMP 320x50 Android", 1538848570905, "Android"),
        ("Banner UMP 320x50 iOS", 1535295950858, "iOS"),
    ]
)
def test_transaction(plc_desc: str, plc: int, os: str):
    mutt_response, _ = client.request(
        env=_ENV, adtype=AdType.BANNER.value, im_plid=plc, os=os, mutt_payload={"a-bid": micro(20)}
    )
    expected_transaction_keys = set(["adSourceName", "buyerName", "buyerPrice", "ctxHash"])
    assert bool(mutt_response), "response shouldn't be empty"
    transaction = mutt_response.ad_sets[0].get("ads")[0].get("transaction")
    assert set(transaction.keys()) == expected_transaction_keys, \
        f"{plc_desc} -- keys mismatch in transactions"
    ad_source_name = transaction.get("adSourceName")
    buyer_name = transaction.get("buyerName")
    buyer_price = transaction.get("buyerPrice")
    assert ad_source_name == "AerMarket AdSource", \
        f"{plc_desc} -- expected 'AerMarket Adsource', but got {ad_source_name} instead"
    assert buyer_name == "InMobi", \
        f"{plc_desc} -- expected 'InMobi', but got {buyer_name} instead"
    assert round(buyer_price, 1) == 11.4, \
        f"{plc_desc} -- expected 11.4, ump ads have a default rev_share=0.57"


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Banner UMP 320x50 Android", 1538848570905, "Android"),
        ("Banner UMP 320x50 iOS", 1535295950858, "iOS"),
    ]
)
def test_tracking_events(plc_desc: str, plc: int, os: str):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        im_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    tracking_events = mutt_response.ad_sets[0].get("ads")[0].get("trackingEvents")
    assert tracking_events == []


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Banner UMP 320x50 Android", 1538848570905, "Android"),
        ("Banner UMP 320x50 iOS", 1535295950858, "iOS"),
    ]
)
def test_asplcid(plc_desc: str, plc: int, os: str):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        im_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    asplcid = mutt_response.ad_sets[0].get("ads")[0].get("asPlcId")
    assert asplcid == 0, \
        f"UMP placement without a mapping AS plc should have asplcid=0"


@pytest.mark.parametrize(
    "plc_desc, plc, os", [
        ("Banner UMP 320x50 Android", 1538848570905, "Android"),
        ("Banner UMP 320x50 iOS", 1535295950858, "iOS"),
    ]
)
def test_applyBitmap(plc_desc: str, plc: int, os: str):
    """
    Always False for UMP or video ads.
    For AS banner ads, 'applyBitmap'=True only when 'canFailover'=True
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        im_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    apply_bitmap = mutt_response.ad_sets[0].get("ads")[0].get("applyBitmap")
    assert apply_bitmap == False, \
        f"{plc_desc} -- expected False, but got {apply_bitmap} instead"
