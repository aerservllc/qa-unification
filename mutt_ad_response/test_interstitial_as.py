import re
import pytest
import uuid
import mutt.request.request_mutt as mrp
from mutt import client
from mutt.common import AdType
from urllib.parse import parse_qs, urlparse
from collections import Counter

_A9_PARAMS_KEY = "mVp40|bidid_A|hostname_A,mVp40|bidid_B|hostname_B"


#-------- close-auction=true --------
@pytest.mark.parametrize(
    "plc_desc, plc", [
        ("S2S Aervideo", 380313),
        ("S2S Aerwall HTML Static", 380629),
        ("AerMarket RTB HTML Static", 380156),
        ("Custom JS HTML Static", 380000),
        ("Custom JS HTML Static Mraid", 380062),
        ("Custom JS HTML Static Mraid Failover", 380063),
        ("Non-dynamic SDK (VC)", 380011),
        ("Non-dynamic SDK (non-VC)", 380012),
        ("A9", 380880),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_all_keys_available(plc_desc: str, plc: int, os: str):
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "as_plid": plc,
        "os": os,
    }
    if plc == 380880:
        params.update({mrp.A9_PARAMS: _A9_PARAMS_KEY})
    mutt_response, _ = client.request(**params)
    expected_root_keys = set(["requestId", "adSets"])
    expected_ad_set_keys = set(["adSetId", "isAuctionClosed", "expiry", "ads"])
    expected_ad_keys = set(
        [
            "pubContent", "tracking", "markupType", "expiry", "impressionId", "canLoadBeforeShow", "metaInfo",
            "transaction", "trackingEvents", "baseEventUrl", "asPlcId"
        ]
    )
    assert bool(mutt_response), "response shouldn't be empty"
    raw_response = mutt_response.raw_response()
    assert set(raw_response.keys()) == expected_root_keys
    adset = raw_response.get("adSets")[0]
    assert set(adset.keys()) == expected_ad_set_keys
    for ad in adset.get("ads"):
        assert set(ad.keys()) >= expected_ad_keys


@pytest.mark.parametrize(
    "plc_desc, plc, expected_partner_code", [
        ("S2S Aervideo", 380313, "aerservnativevideo709050655357"),
        ("S2S Aerwall HTML Static", 380629, "aerservdisplay599566902305"),
        ("AerMarket RTB HTML Static", 380156, "aerservdisplay599566902305"),
        ("Custom JS HTML Static", 380000, "aerservdisplay599566902305"),
        ("Custom JS HTML Static Mraid", 380062, "aerservdisplay599566902305"),
        ("Custom JS HTML Static Mraid Failover", 380063, "aerservdisplay599566902305"),
        ("Non-dynamic SDK (VC)", 380011, "aerservdisplay599566902305"),
        ("Non-dynamic SDK (non-VC)", 380012, "aerservdisplay599566902305"),
        ("A9", 380880, "aerservnativevideo709050655357"),
        ("AerMarket Mobile Vast Moat Enabled", 380818, "aerservnativevideo709050655357"),
        ("RTB VAST In App AT 2", 380155, "aerservnativevideo709050655357"),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_viewability(plc_desc: str, plc: int, expected_partner_code: str, os: str):
    """
    Logic for viewability internal enablement:
    Only AerMarket Mediation(BANNER or VAST) can have it True if:
     - ad_line.is_moat_enabled=True
     - rtb response has moat.com as a vendor
    """
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "as_plid": plc,
        "os": os,
    }
    expected_client_levels = []
    expected_client_slicers = []

    if plc == 380880:
        params.update({mrp.A9_PARAMS: _A9_PARAMS_KEY})
    if plc == 380155:
        params.update({"adid": "test_inmobi_bid_with_viewabilityvendor_and_experience_and_rewards_moatenabled"})
        expected_client_levels.extend(["AerMarketVastRtbMobile", "1000", "380038", "380017"])
        expected_client_slicers.extend([f"sample {os} app".lower(), "380155"])
    if plc == 380818:
        expected_client_levels.extend(["AerMarketVastRtbMobile", "1000", "380258", "380017"])
        expected_client_slicers.extend([f"sample {os} app".lower(), "380818"])

    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    viewability = mutt_response.ad_sets[0].get("ads")[0].get("viewability")[0]
    assert viewability, "viewability object should exist"
    moat = viewability.get("moat")
    assert moat.get("instrumentVideo") is False
    assert moat.get("enabled") is bool(expected_client_levels)
    assert moat.get("partnerCode") == expected_partner_code
    assert moat.get("clientLevels") == expected_client_levels
    assert moat.get("clientSlicers") == expected_client_slicers


@pytest.mark.parametrize(
    "plc_desc, plc, expected_is_auction_closed", [
        ("S2S Aervideo", 380313, True),
        ("S2S Aerwall HTML Static", 380629, True),
        ("AerMarket RTB HTML Static", 380156, True),
        ("Custom JS HTML Static", 380000, True),
        ("Custom JS HTML Static Mraid", 380062, True),
        ("Custom JS HTML Static Mraid Failover", 380063, True),
        ("Non-dynamic SDK (VC)", 380011, True),
        ("Non-dynamic SDK (non-VC)", 380012, True),
        ("A9", 380880, True),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_isAuctionClosed(plc_desc: str, plc: int, expected_is_auction_closed: bool, os: str):
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "as_plid": plc,
        "os": os,
    }
    if plc == 380880:
        params.update({mrp.A9_PARAMS: _A9_PARAMS_KEY})
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    is_auction_closed = mutt_response.ad_sets[0].get("isAuctionClosed")
    assert is_auction_closed == expected_is_auction_closed, \
        f"{plc_desc} -- expected {expected_is_auction_closed}, but got {is_auction_closed} instead"


@pytest.mark.parametrize(
    "plc_desc, plc", [
        ("S2S Aervideo", 380313),
        ("S2S Aerwall HTML Static", 380629),
        ("AerMarket RTB HTML Static", 380156),
        ("Custom JS HTML Static", 380000),
        ("Custom JS HTML Static Mraid", 380062),
        ("Custom JS HTML Static Mraid Failover", 380063),
        ("Non-dynamic SDK (VC)", 380011),
        ("Non-dynamic SDK (non-VC)", 380012),
        ("A9", 380880),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_tracking(plc_desc: str, plc: int, os: str):
    """
    Tracking type:
    Web: when serving video 3.0. Else, unknown.
    Only UMP supports video 3.0 as of now. AS is currently being served as video 2.0
    """
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "as_plid": plc,
        "os": os,
    }
    if plc == 380880:
        params.update({mrp.A9_PARAMS: _A9_PARAMS_KEY})
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    tracking = mutt_response.ad_sets[0].get("ads")[0].get("tracking")
    assert tracking == "unknown", \
        f"{plc_desc} -- expected 'unknown', but got {tracking} instead"


@pytest.mark.parametrize(
    "plc_desc, plc, expected_markup_type", [
        ("S2S Aervideo", 380313, "inmobiJson"),
        ("S2S Aerwall HTML Static", 380629, "html"),
        ("AerMarket RTB HTML Static", 380156, "html"),
        ("Custom JS HTML Static", 380000, "html"),
        ("Custom JS HTML Static Mraid", 380062, "html"),
        ("Custom JS HTML Static Mraid Failover", 380063, "html"),
        ("Non-dynamic SDK (VC)", 380011, "mediationJson"),
        ("Non-dynamic SDK (non-VC)", 380012, "mediationJson"),
        ("A9", 380880, "inmobiJson"),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_markupType(plc_desc: str, plc: int, expected_markup_type: str, os: str):
    """
    The possible values that Mutt sends today are: html, inmobiJson and mediationJson,
    """
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "as_plid": plc,
        "os": os,
    }
    if plc == 380880:
        params.update({mrp.A9_PARAMS: _A9_PARAMS_KEY})
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    markup_type = mutt_response.ad_sets[0].get("ads")[0].get("markupType")
    assert markup_type == expected_markup_type, \
        f"{plc_desc} -- expected {expected_markup_type}, but got {markup_type} instead"


@pytest.mark.parametrize(
    "plc_desc, plc", [
        ("S2S Aervideo", 380313),
        ("S2S Aerwall HTML Static", 380629),
        ("AerMarket RTB HTML Static", 380156),
        ("Custom JS HTML Static", 380000),
        ("Custom JS HTML Static Mraid", 380062),
        ("Custom JS HTML Static Mraid Failover", 380063),
        ("Non-dynamic SDK (VC)", 380011),
        ("Non-dynamic SDK (non-VC)", 380012),
        ("A9", 380880),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_expiry(plc_desc: str, plc: int, os: str):
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "as_plid": plc,
        "os": os,
    }
    if plc == 380880:
        params.update({mrp.A9_PARAMS: _A9_PARAMS_KEY})
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    expiry = mutt_response.ad_sets[0].get("ads")[0].get("expiry")
    assert expiry == 600, "default value of expiry should be 600"


@pytest.mark.parametrize(
    "plc_desc, plc", [
        ("S2S Aervideo", 380313),
        ("S2S Aerwall HTML Static", 380629),
        ("AerMarket RTB HTML Static", 380156),
        ("Custom JS HTML Static", 380000),
        ("Custom JS HTML Static Mraid", 380062),
        ("Custom JS HTML Static Mraid Failover", 380063),
        ("Non-dynamic SDK (VC)", 380011),
        ("Non-dynamic SDK (non-VC)", 380012),
        ("A9", 380880),
    ]
)
def test_safeArea(plc_desc: str, plc: int):
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "as_plid": plc,
        "os": "iOS",
        mrp.IOS_DEVICE_MACHINE_HARDWARE: "iPhone10,3",
    }
    if plc == 380880:
        params.update({mrp.A9_PARAMS: _A9_PARAMS_KEY})
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    safe_area = mutt_response.ad_sets[0].get("ads")[0].get("safeArea")
    if plc in [380011, 380012]:
        assert safe_area is None, \
            "safeArea should not be populated for mediation json"
    else:
        expected_safe_area_keys = ["assetStyle"]
        expected_asset_style_keys = ["nonSafeAreaBackgroundColor", "geometry"]
        assert set(safe_area.keys()) ==set(expected_safe_area_keys)
        assert set(safe_area.get("assetStyle").keys()) ==set(expected_asset_style_keys)


@pytest.mark.parametrize(
    "plc_desc, plc, expected_can_load_before_show", [
        ("S2S Aervideo", 380313, True),
        ("S2S Aerwall HTML Static", 380629, False),
        ("AerMarket RTB HTML Static", 380156, False),
        ("Custom JS HTML Static", 380000, False),
        ("Custom JS HTML Static Mraid", 380062, False),
        ("Custom JS HTML Static Mraid Failover", 380063, False),
        ("Non-dynamic SDK (VC)", 380011, True),
        ("Non-dynamic SDK (non-VC)", 380012, True),
        ("A9", 380880, True),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_canLoadBeforeShow(plc_desc: str, plc: int, expected_can_load_before_show: bool, os: str):
    """
    AerServ's Ad responses cannot be loaded in the webview before the show is invoked. But UMP's ads can.
    This flag determines if the ad response can be loaded in the webview before the show is invoked or not.
    UMP ad: true
    AerMarket ad: true for markupType != html, false otherwise
    Mediation ad: true for native and interstitial. false for banner
    """
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "as_plid": plc,
        "os": os,
    }
    if plc == 380880:
        params.update({mrp.A9_PARAMS: _A9_PARAMS_KEY})
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    can_load_before_show = mutt_response.ad_sets[0].get("ads")[0].get("canLoadBeforeShow")
    assert can_load_before_show == expected_can_load_before_show, \
        "canLoadBeforeShow should be False for all banner ads(type=3) and non-sdk html"


@pytest.mark.parametrize(
    "plc_desc, plc, expected_creative_type", [
        ("S2S Aervideo", 380313, "video"),
        ("S2S Aerwall HTML Static", 380629, "nonvideo"),
        ("AerMarket RTB HTML Static", 380156, "nonvideo"),
        ("Custom JS HTML Static", 380000, "nonvideo"),
        ("Custom JS HTML Static Mraid", 380062, "nonvideo"),
        ("Custom JS HTML Static Mraid Failover", 380063, "nonvideo"),
        ("Non-dynamic SDK (VC)", 380011, "unknown"),
        ("Non-dynamic SDK (non-VC)", 380012, "unknown"),
        ("A9", 380880, "video"),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_metaInfo(plc_desc: str, plc: int, expected_creative_type: str, os: str):
    """
    omidEnabled will be true if OMSDK needs to be initialized by our SDK (dictated by UMP demand).
    customReferenceData and isolateVerificationScripts are today sent as empty string and false,
    respectively from server side. These are also required by SDK for OMSDK init.
    Only supported for UMP. For AS, this will have "omidEnabled: false".
    Structure:
    {
    "omidEnabled": "true/false",
    "customReferenceData": "some-string",
    "isolateVerificationScripts": "true/false",
    "macros": {
        "key": "value",
        "key2": "value2"
    }
    """
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "as_plid": plc,
        "os": os,
    }
    if plc == 380880:
        params.update({mrp.A9_PARAMS: _A9_PARAMS_KEY})
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    expected_metainfo_keys = set(["creativeType", "omsdkInfo"])
    meta_info = mutt_response.ad_sets[0].get("ads")[0].get("metaInfo")
    assert set(meta_info.keys()) == expected_metainfo_keys
    assert meta_info.get("creativeType") == expected_creative_type
    assert meta_info.get("omsdkInfo").get("omidEnabled") == False
    assert meta_info.get("omsdkInfo").get("isolateVerificationScripts") == False


@pytest.mark.parametrize(
    "plc_desc, plc, expected_key_value", [
        ("S2S Aervideo", 380313, {
            "buyerPrice": 0,
            "adSourceName": "Smaato S2S Video",
            "buyerName": "SmaatoVast",
        }),
        (
            "S2S Aerwall HTML Static", 380629, {
                "buyerPrice": 0.7,
                "adSourceName": "AdServer - Response Validation - Pubmatic Mobile - AdSource",
                "buyerName": "ProxiedNetworkWrapper:PubmaticMobile",
            }
        ),
        (
            "AerMarket RTB HTML Static", 380156, {
                "buyerPrice": 1,
                "adSourceName": "AerMarket Test Page",
                "buyerName": "AerMarket",
                "dspId": "DSPMock",
                "adomain": "aerserv.com",
            }
        ),
        (
            "Custom JS HTML Static", 380000, {
                "buyerPrice": 5,
                "adSourceName": "AerMarket Test Page",
                "buyerName": "AerMarket",
            }
        ),
        (
            "Custom JS HTML Static Mraid", 380062, {
                "buyerPrice": 5,
                "adSourceName": "AerMarket Test Page",
                "buyerName": "AerMarket",
            }
        ),
        (
            "Custom JS HTML Static Mraid Failover", 380063, {
                "buyerPrice": 5,
                "adSourceName": "AerMarket Test Page",
                "buyerName": "AerMarket",
            }
        ),
        ("Non-dynamic SDK (VC)", 380011, {
            "buyerPrice": 1,
            "adSourceName": "iOS APPLOVIN API Reporting",
            "buyerName": "AppLovin",
        }),
        (
            "Non-dynamic SDK (non-VC)", 380012, {
                "buyerPrice": 1,
                "adSourceName": "iOS APPLOVIN",
                "buyerName": "AppLovin",
            }
        ),
        (
            "A9", 380880, {
                "buyerPrice": 5,
                "adSourceName": "A9 Video Ad Source with Matching pricepoint",
                "buyerName": "A9Vast",
            }
        ),
    ]
)
def test_transaction(plc_desc: str, plc: int, expected_key_value: dict):
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "as_plid": plc,
        "os": "iOS",
    }
    if plc == 380880:
        params.update({mrp.A9_PARAMS: _A9_PARAMS_KEY})
    mutt_response, _ = client.request(**params)
    expected_transaction_keys = set(expected_key_value.keys())
    expected_transaction_keys.add("ctxHash")
    assert bool(mutt_response), "response shouldn't be empty"
    transaction = mutt_response.ad_sets[0].get("ads")[0].get("transaction")
    assert set(transaction.keys()) == expected_transaction_keys, \
        f"{plc_desc} -- keys mismatch in transactions"
    for key in expected_key_value:
        expected_value = expected_key_value.get(key)
        result_value = transaction.get(key)
        assert expected_value == result_value, \
            f"{plc_desc} -- expected {expected_value}, but got {result_value} instead"


@pytest.mark.parametrize(
    "plc_desc, plc, expected_tracking_ev_types",
    [
        ("S2S Aervideo", 380313, []),
        ("S2S Aerwall HTML Static", 380629, ["banner_rendered"]),
        ("AerMarket RTB HTML Static", 380156, ["banner_rendered"]),
        ("Custom JS HTML Static", 380000, ["banner_rendered"]),
        ("Custom JS HTML Static Mraid", 380062, [
            "vast_impression", "banner_rendered"
        ]),  # the reason it has vast_impression and banner_rendered is it is Mraid
        ("Custom JS HTML Static Mraid Failover", 380063, [
            "vast_impression", "banner_rendered"
        ]),  # the reason it has vast_impression and banner_rendered is it is Mraid
        ("Non-dynamic SDK (VC)", 380011, ["sdk_impression", "sdk_failure", "sdk_attempt"]),
        ("Non-dynamic SDK (non-VC)", 380012, ["sdk_impression", "sdk_failure", "sdk_attempt"]),
        ("A9", 380880, []),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_tracking_events(plc_desc: str, plc: int, expected_tracking_ev_types: list, os: str):
    """
    380062 and 380063 have both vast_impression and banner_rendered because it is Mraid
    """
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "as_plid": plc,
        "os": os,
    }
    if plc == 380880:
        params.update({mrp.A9_PARAMS: _A9_PARAMS_KEY})
    mutt_response, _ = client.request(**params)
    expected_tracking_events_keys = ["type", "trackingUrls"]
    expected_tracking_params = [
        "plc", "rid", "pubid", "appid", "gdpr", "gdpr_consent", "os", "adid", "sdkv", "iline", "asplcid", "asid",
        "pinned", "vc_user_id", "hck", "xcid", "ntid", "ctxhash", "eu", "ev"
    ]
    optional_tracking_params = ["rtbdspid", "rtbbidid", "rtbrid", "campaignid", "buyer", "icid", "pchain"]
    assert bool(mutt_response), "response shouldn't be empty"
    tracking_events = mutt_response.ad_sets[0].get("ads")[0].get("trackingEvents")
    assert len(tracking_events) == len(expected_tracking_ev_types)
    for tracking in tracking_events:
        assert set(tracking.keys()) ==set(expected_tracking_events_keys)
        assert tracking.get("type") in expected_tracking_ev_types
        tracking_params = parse_qs(urlparse(tracking.get("trackingUrls")[0]).query, True)
        assert all([len(value) == 1 for value in tracking_params.values()]), \
            "there shouldn't be any duplicate keys"
        extra_keys = set(tracking_params.keys()).difference(set(expected_tracking_params))
        optional_keys = extra_keys.issubset(set(optional_tracking_params))
        assert len(extra_keys) == 0 or optional_keys


@pytest.mark.parametrize(
    "plc_desc, plc", [
        ("S2S Aervideo", 380313),
        ("S2S Aerwall HTML Static", 380629),
        ("AerMarket RTB HTML Static", 380156),
        ("Custom JS HTML Static", 380000),
        ("Custom JS HTML Static Mraid", 380062),
        ("Custom JS HTML Static Mraid Failover", 380063),
        ("Non-dynamic SDK (VC)", 380011),
        ("Non-dynamic SDK (non-VC)", 380012),
        ("A9", 380880),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_base_event_url(plc_desc: str, plc: int, os: str):
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "as_plid": plc,
        "os": os,
    }
    if plc == 380880:
        params.update({mrp.A9_PARAMS: _A9_PARAMS_KEY})
    mutt_response, _ = client.request(**params)
    expected_parameters_in_base_event_url = [
        "plc", "rid", "pubid", "appid", "gdpr", "gdpr_consent", "os", "adid", "sdkv", "iline", "asplcid", "asid",
        "campaignid", "pinned", "vc_user_id", "buyer", "hck", "icid", "xcid", "ntid", "ctxhash", "eu", "ev", "pchain"
    ]
    assert bool(mutt_response), "response shouldn't be empty"
    base_event_url = mutt_response.ad_sets[0].get("ads")[0].get("baseEventUrl")
    event_params = parse_qs(urlparse(base_event_url).query, True)
    assert Counter(event_params.keys()) == Counter(expected_parameters_in_base_event_url)
    for param in ["ctxhash", "eu", "ev"]:
        assert event_params[param] == ["${" + param + "}"]


@pytest.mark.parametrize(
    "plc_desc, plc, expected_asplcid", [
        ("S2S Aervideo", 380313, 380544),
        ("S2S Aerwall HTML Static", 380629, 380834),
        ("AerMarket RTB HTML Static", 380156, 380282),
        ("Custom JS HTML Static", 380000, 380000),
        ("Custom JS HTML Static Mraid", 380062, 380131),
        ("Custom JS HTML Static Mraid Failover", 380063, 380132),
        ("Non-dynamic SDK (VC)", 380011, 380017),
        ("Non-dynamic SDK (non-VC)", 380012, 380019),
        ("A9", 380880, 381309),
    ]
)
def test_asplcid(plc_desc: str, plc: int, expected_asplcid: int):
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "as_plid": plc,
        "os": "iOS",
    }
    if plc == 380880:
        params.update({mrp.A9_PARAMS: _A9_PARAMS_KEY})
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    asplcid = mutt_response.ad_sets[0].get("ads")[0].get("asPlcId")
    assert asplcid == expected_asplcid, \
        f"{plc_desc} -- expected {expected_asplcid}, but got {asplcid} instead"


@pytest.mark.parametrize(
    "plc_desc, plc, expected_rewards", [
        ("Non-dynamic SDK (VC)", 380011, {
            "name": "Money",
            "rewardAmount": 42
        }),
        ("Non-dynamic SDK (non-VC)", 380012, None),
        ("VAST - VC", 380004, {
            "name": "Money",
            "rewardAmount": 42
        }),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_rewards(plc_desc: str, plc: int, expected_rewards: dict or None, os: str):
    """
    inmobiJson should have one rewards under pubContent and one under pubContent.rootContainer.assetValue(UMP-SDK legacy)
    mediationJson should only have one rewards outside pubContent. (the non-dynamic sdk plcs are mediationJson)
    """
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "as_plid": plc,
        "os": os,
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    markup_type = mutt_response.ad_sets[0].get("ads")[0].get("markupType")
    if markup_type == "mediationJson":
        rewards = mutt_response.ad_sets[0].get("ads")[0].get("rewards")
        assert rewards == expected_rewards, \
            f"{plc_desc} -- expected {expected_rewards}, but got {rewards} instead"
    if markup_type == "inmobiJson":
        pub_content = mutt_response.ad_sets[0].get("ads")[0].get("pubContent")
        rewards_1 = pub_content.get("rewards")
        rewards_2 = list(
            filter(
                None,
                [asset_value.get("rewards") for asset_value in pub_content.get("rootContainer").get("assetValue")]
            )
        )[0]
        assert rewards_1 == expected_rewards
        assert rewards_2 == expected_rewards


@pytest.mark.parametrize(
    "plc_desc, plc, expected_apply_bitmap", [
        ("RTB VAST In App AT 2", 380155, None),
        ("S2S Aervideo", 380313, None),
        ("S2S Aerwall HTML Static", 380629, False),
        ("AerMarket RTB HTML Static", 380156, False),
        ("Custom JS HTML Static", 380000, True),
        ("Custom JS HTML Static Mraid", 380062, True),
        ("Custom JS HTML Static Mraid Failover", 380063, True),
        ("Non-dynamic SDK (VC)", 380011, None),
        ("Non-dynamic SDK (non-VC)", 380012, None),
        ("A9", 380880, None),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_applyBitmap(plc_desc: str, plc: int, expected_apply_bitmap: bool or None, os: str):
    """
    Always False for UMP or video ads.
    For AS banner ads, 'applyBitmap'=True only when 'canFailover'=True
    """
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "as_plid": plc,
        "os": os,
    }
    if plc == 380880:
        params.update({mrp.A9_PARAMS: _A9_PARAMS_KEY})
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    for ad in iter(mutt_response.ad_sets[0].ads):
        apply_bitmap = ad.get("applyBitmap")
        assert apply_bitmap == expected_apply_bitmap, \
            f"{plc_desc} -- expected {expected_apply_bitmap}, but got {applyBitmap} instead"


@pytest.mark.parametrize(
    "plc_desc, os, as_plcid, adid", [
        ("iOS - 1 Non-Dynamic Interstital Vast SDK + 2 Dynamic Interstitial Vast SDK", "Android", 380744, None),
        ("Android - 1 Non-Dynamic Interstital Vast SDK + 2 Dynamic Interstitial Vast SDK", "iOS", 380744, None),
    ]
)
def test_non_close_auction_response(
    plc_desc: str,
    os: str,
    as_plcid: int,
    adid: str or None,
):
    adid = (adid or str(uuid.uuid4()))
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        as_plid=as_plcid,
        os=os,
        adid=adid,
        has_dynamic_mediation=True,
    )
    assert len(mutt_response) == 3, "There should be 1 non_dynamic sdk and 2 dynamic sdk"
    assert len(mutt_response.ad_sets) == 1

    ad_set = mutt_response.ad_sets[0]
    assert set(ad_set.keys()) == {"adSetId", "expiry", "isAuctionClosed", "adSetAuctionMeta", "ads"}
    assert ad_set.get("adSetAuctionMeta") is not None
    for ad in mutt_response.ad_sets[0].ads:
        assert set(ad.keys()) == {
            "pubContent",
            "tracking",
            "viewability",
            "markupType",
            "expiry",
            "impressionId",
            "canLoadBeforeShow",
            "metaInfo",
            "transaction",
            "trackingEvents",
            "baseEventUrl",
            "asPlcId",
            "adAuctionMeta",
        }
        assert ad.get("adAuctionMeta") is not None
        transaction = ad.get("transaction")
        assert set(transaction.keys()) >= {"buyerName", "adSourceName"}
        assert "ctxHash" not in transaction and "buyerPrice" not in transaction
