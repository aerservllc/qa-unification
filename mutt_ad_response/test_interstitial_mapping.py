import re
import pytest
import mutt.request.request_mutt as mrp
import verify_tracking_events as vte
from urllib.parse import parse_qs, urlparse
from mutt import client
from mutt.common import AdType
from collections import Counter
from mutt.request.request_mutt import micro

_A_BID_UMP_WIN = micro(20)
_A_BID_UMP_LOSS = micro(1)


#-------- close-auction=true --------
@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, os, a_bid", [
        ("Interstitial Mirror Android - UMP WIN", 380991, 1539906510215, "Android", _A_BID_UMP_WIN),
        ("Interstitial Mirror iOS - UMP WIN", 380991, 1535339526186, "iOS", _A_BID_UMP_WIN),
        ("Interstitial Mirror Android - UMP LOSS", 380991, 1539906510215, "Android", _A_BID_UMP_LOSS),
        ("Interstitial Mirror iOS - UMP LOSS", 380991, 1535339526186, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_all_keys_available(plc_desc: str, as_plid: int, im_plid: int, os: str, a_bid: int):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        as_plid=as_plid,
        im_plid=im_plid,
        os=os,
        mutt_payload={"a-bid": a_bid}
    )
    expected_root_keys = set(["requestId", "adSets"])
    expected_ad_set_keys = set(["adSetId", "isAuctionClosed", "expiry", "ads"])
    expected_ad_keys = set(
        [
            "pubContent", "tracking", "markupType", "expiry", "impressionId", "canLoadBeforeShow", "metaInfo",
            "transaction", "trackingEvents", "baseEventUrl", "asPlcId"
        ]
    )
    assert bool(mutt_response), "response shouldn't be empty"
    raw_response = mutt_response.raw_response()
    assert set(raw_response.keys()) == expected_root_keys
    adset = raw_response.get("adSets")[0]
    assert set(adset.keys()) == expected_ad_set_keys
    for ad in adset.get("ads"):
        assert set(ad.keys()) >= expected_ad_keys


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, expected_is_auction_closed, os, a_bid", [
        ("Interstitial Mirror Android - UMP WIN", 380991, 1539906510215, True, "Android", _A_BID_UMP_WIN),
        ("Interstitial Mirror iOS - UMP WIN", 380991, 1535339526186, True, "iOS", _A_BID_UMP_WIN),
        ("Interstitial Mirror Android - UMP LOSS", 380991, 1539906510215, True, "Android", _A_BID_UMP_LOSS),
        ("Interstitial Mirror iOS - UMP LOSS", 380991, 1535339526186, True, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_isAuctionClosed(
    plc_desc: str, as_plid: int, im_plid: int, expected_is_auction_closed: bool, os: str, a_bid: int
):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        as_plid=as_plid,
        im_plid=im_plid,
        os=os,
        mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    is_auction_closed = mutt_response.ad_sets[0].get("isAuctionClosed")
    assert is_auction_closed == expected_is_auction_closed, \
        f"{plc_desc} -- expected {expected_is_auction_closed}, but got {isAuctionClosed} instead"


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, expected_tracking, os, a_bid", [
        ("Interstitial Mirror Android - UMP WIN", 380991, 1539906510215, "unknown", "Android", _A_BID_UMP_WIN),
        ("Interstitial Mirror iOS - UMP WIN", 380991, 1535339526186, "unknown", "iOS", _A_BID_UMP_WIN),
        ("Interstitial Mirror Android - UMP LOSS", 380991, 1539906510215, "unknown", "Android", _A_BID_UMP_LOSS),
        ("Interstitial Mirror iOS - UMP LOSS", 380991, 1535339526186, "unknown", "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_tracking(plc_desc: str, as_plid: int, im_plid: int, expected_tracking: str, os: str, a_bid: int):
    """
    Tracking type:
    Web: when serving video 3.0. Else, unknown.
    Only UMP supports video 3.0 as of now. AS is currently being served as video 2.0
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        as_plid=as_plid,
        im_plid=im_plid,
        os=os,
        mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    tracking = mutt_response.ad_sets[0].get("ads")[0].get("tracking")
    assert tracking == expected_tracking, \
        f"{plc_desc} -- expected {expected_tracking}, but got {tracking} instead"


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, expected_markup_type, os, a_bid", [
        (
            "Interstitial Mirror Android - UMP WIN", 380991, 1539906510215, ["html", "inmobiJson"], "Android",
            _A_BID_UMP_WIN
        ),
        ("Interstitial Mirror iOS - UMP WIN", 380991, 1535339526186, ["html", "inmobiJson"], "iOS", _A_BID_UMP_WIN),
        ("Interstitial Mirror Android - UMP LOSS", 380991, 1539906510215, ["inmobiJson"], "Android", _A_BID_UMP_LOSS),
        ("Interstitial Mirror iOS - UMP LOSS", 380991, 1535339526186, ["inmobiJson"], "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_markupType(plc_desc: str, as_plid: int, im_plid: int, expected_markup_type: list, os: str, a_bid: int):
    """
    The possible values that Mutt sends today are: html, inmobiJson and mediationJson,
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        as_plid=as_plid,
        im_plid=im_plid,
        os=os,
        mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    markup_type = mutt_response.ad_sets[0].get("ads")[0].get("markupType")
    assert markup_type in expected_markup_type, \
        f"{plc_desc} -- expected {expected_markup_type}, but got {markup_type} instead"


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, expected_expiry, os, a_bid", [
        ("Interstitial Mirror Android - UMP WIN", 380991, 1539906510215, 9000, "Android", _A_BID_UMP_WIN),
        ("Interstitial Mirror iOS - UMP WIN", 380991, 1535339526186, 9000, "iOS", _A_BID_UMP_WIN),
        ("Interstitial Mirror Android - UMP LOSS", 380991, 1539906510215, 600, "Android", _A_BID_UMP_LOSS),
        ("Interstitial Mirror iOS - UMP LOSS", 380991, 1535339526186, 600, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_expiry(plc_desc: str, as_plid: int, im_plid: int, expected_expiry: int, os: str, a_bid: int):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        as_plid=as_plid,
        im_plid=im_plid,
        os=os,
        mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    expiry = mutt_response.ad_sets[0].get("ads")[0].get("expiry")
    assert expiry == expected_expiry, \
        f"{plc_desc} -- expected {expected_expiry}, but got {expiry} instead"


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, a_bid", [
        ("Interstitial Mirror iOS - UMP WIN", 380991, 1535339526186, _A_BID_UMP_WIN),
        ("Interstitial Mirror iOS - UMP LOSS", 380991, 1535339526186, _A_BID_UMP_LOSS),
    ]
)
def test_safeArea(plc_desc: str, as_plid: int, im_plid: int, a_bid: int):
    params = {
        "env": _ENV,
        "adtype": AdType.INTERSTITIAL.value,
        "as_plid": as_plid,
        "im_plid": im_plid,
        "os": "iOS",
        mrp.IOS_DEVICE_MACHINE_HARDWARE: "iPhone10,3",
        "a-bid": a_bid
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    safe_area = mutt_response.ad_sets[0].get("ads")[0].get("safeArea")
    expected_safe_area_keys = ["assetStyle"]
    expected_asset_style_keys = ["nonSafeAreaBackgroundColor", "geometry"]
    assert set(safe_area.keys()) ==set(expected_safe_area_keys)
    assert set(safe_area.get("assetStyle").keys()) ==set(expected_asset_style_keys)


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, has_creative_id, os, a_bid", [
        ("Interstitial Mirror Android - UMP WIN", 380991, 1539906510215, True, "Android", _A_BID_UMP_WIN),
        ("Interstitial Mirror iOS - UMP WIN", 380991, 1535339526186, True, "iOS", _A_BID_UMP_WIN),
        ("Interstitial Mirror Android - UMP LOSS", 380991, 1539906510215, False, "Android", _A_BID_UMP_LOSS),
        ("Interstitial Mirror iOS - UMP LOSS", 380991, 1535339526186, False, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_creativeId(plc_desc: str, as_plid: int, im_plid: int, has_creative_id: bool, os: str, a_bid: int):
    """
    Encrypted creative Id of the ad. Passed on to the publisher.
    This will currently exist only for IM ads.
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        as_plid=as_plid,
        im_plid=im_plid,
        os=os,
        mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    creative_id = mutt_response.ad_sets[0].get("ads")[0].get("creativeId")
    assert bool(creative_id) == has_creative_id, "creativeId should exist for UMP ads but not for AS ads"


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, expected_can_load_before_show, os, a_bid", [
        ("Interstitial Mirror Android - UMP WIN", 380991, 1539906510215, True, "Android", _A_BID_UMP_WIN),
        ("Interstitial Mirror iOS - UMP WIN", 380991, 1535339526186, True, "iOS", _A_BID_UMP_WIN),
        ("Interstitial Mirror Android - UMP LOSS", 380991, 1539906510215, True, "Android", _A_BID_UMP_LOSS),
        ("Interstitial Mirror iOS - UMP LOSS", 380991, 1535339526186, True, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_canLoadBeforeShow(
    plc_desc: str, as_plid: int, im_plid: int, expected_can_load_before_show: bool, os: str, a_bid: int
):
    """
    AerServ's Ad responses cannot be loaded in the webview before the show is invoked. But UMP's ads can. This flag determines if the ad response can be loaded in the webview before the show is invoked or not.
    UMP ad: true
    AerMarket ad: true for markupType != html, false otherwise
    Mediation ad: true for native and interstitial. false for banner
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        as_plid=as_plid,
        im_plid=im_plid,
        os=os,
        mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    can_load_before_show = mutt_response.ad_sets[0].get("ads")[0].get("canLoadBeforeShow")
    assert can_load_before_show == expected_can_load_before_show, \
        f"{plc_desc} -- expected {expected_can_load_before_show}, but got {can_load_before_show} instead"


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, os, a_bid", [
        ("Interstitial Mirror Android - UMP WIN", 380991, 1539906510215, "Android", _A_BID_UMP_WIN),
        ("Interstitial Mirror iOS - UMP WIN", 380991, 1535339526186, "iOS", _A_BID_UMP_WIN),
        ("Interstitial Mirror Android - UMP LOSS", 380991, 1539906510215, "Android", _A_BID_UMP_LOSS),
        ("Interstitial Mirror iOS - UMP LOSS", 380991, 1535339526186, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_metaInfo(plc_desc: str, as_plid: int, im_plid: int, os: str, a_bid: int):
    """
    omidEnabled will be true if OMSDK needs to be initialized by our SDK (dictated by UMP demand).
    customReferenceData and isolateVerificationScripts are today sent as empty string and false,
    respectively from server side. These are also required by SDK for OMSDK init.
    Only supported for UMP. For AS, this will have "omidEnabled: false".
    Structure:
    {
    "omidEnabled": "true/false",
    "customReferenceData": "some-string",
    "isolateVerificationScripts": "true/false",
    "macros": {
        "key": "value",
        "key2": "value2"
    }
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        as_plid=as_plid,
        im_plid=im_plid,
        os=os,
        mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    expected_metainfo_keys = set(["creativeType", "omsdkInfo"])
    meta_info = mutt_response.ad_sets[0].get("ads")[0].get("metaInfo")
    assert set(meta_info.keys()) == expected_metainfo_keys
    assert meta_info.get("creativeType") in ["unknown", "nonvideo", "video"]
    assert meta_info.get("omsdkInfo").get("omidEnabled") == False
    assert meta_info.get("omsdkInfo").get("customReferenceData") == ("" if "UMP WIN" in plc_desc else None)
    assert meta_info.get("omsdkInfo").get("isolateVerificationScripts") == False


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, os, a_bid", [
        ("Interstitial Mirror Android - UMP WIN", 380991, 1539906510215, "Android", _A_BID_UMP_WIN),
        ("Interstitial Mirror iOS - UMP WIN", 380991, 1535339526186, "iOS", _A_BID_UMP_WIN),
        ("Interstitial Mirror Android - UMP LOSS", 380991, 1539906510215, "Android", _A_BID_UMP_LOSS),
        ("Interstitial Mirror iOS - UMP LOSS", 380991, 1535339526186, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_transaction(plc_desc: str, as_plid: int, im_plid: int, os: str, a_bid: int):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        as_plid=as_plid,
        im_plid=im_plid,
        os=os,
        mutt_payload={"a-bid": a_bid}
    )
    expected_transaction_keys = set(["adSourceName", "buyerName", "buyerPrice", "ctxHash"])
    assert bool(mutt_response), "response shouldn't be empty"
    transaction = mutt_response.ad_sets[0].get("ads")[0].get("transaction")
    if "UMP LOSS" in plc_desc:
        expected_transaction_keys.update(set(["dspId", "adomain"]))
    assert set(transaction.keys()) == expected_transaction_keys, \
        f"{plc_desc} -- keys mismatch in transactions"
    ad_source_name = transaction.get("adSourceName")
    buyer_name = transaction.get("buyerName")
    buyer_price = transaction.get("buyerPrice")
    if "UMP WIN" in plc_desc:
        assert ad_source_name == "AerMarket AdSource"
        assert buyer_name == "InMobi"
        assert round(buyer_price, 1) == 11.4
    else:
        dsp_id = transaction.get("dspId")
        adomain = transaction.get("adomain")
        assert ad_source_name == "AerMarket Test Page"
        assert buyer_name == "AerMarket"
        assert dsp_id == "DSPMock"
        assert adomain == "aerserv.com"
        assert abs(buyer_price - 1.806) <= 0.01


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, os, a_bid", [
        ("Interstitial Mirror Android - UMP WIN", 380991, 1539906510215, "Android", _A_BID_UMP_WIN),
        ("Interstitial Mirror iOS - UMP WIN", 380991, 1535339526186, "iOS", _A_BID_UMP_WIN),
        ("Interstitial Mirror Android - UMP LOSS", 380991, 1539906510215, "Android", _A_BID_UMP_LOSS),
        ("Interstitial Mirror iOS - UMP LOSS", 380991, 1535339526186, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_base_event_url(plc_desc: str, as_plid: int, im_plid: int, os: str, a_bid: int):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        as_plid=as_plid,
        im_plid=im_plid,
        os=os,
        mutt_payload={"a-bid": a_bid}
    )
    expected_parameters_in_base_event_url = [
        "plc", "rid", "pubid", "appid", "gdpr", "gdpr_consent", "os", "adid", "sdkv", "iline", "asplcid", "asid",
        "campaignid", "pinned", "vc_user_id", "buyer", "icid", "ntid", "ctxhash", "eu", "ev", "pchain"
    ]
    if "UMP LOSS" in plc_desc:
        expected_parameters_in_base_event_url.extend(["hck", "xcid"])
    assert len(mutt_response), "response shouldn't be empty"
    base_event_url = mutt_response.ad_sets[0].get("ads")[0].get("baseEventUrl")
    event_params = parse_qs(urlparse(base_event_url).query, True)
    assert Counter(event_params.keys()) == Counter(expected_parameters_in_base_event_url)
    for param in ["ctxhash", "eu", "ev"]:
        assert event_params[param] == ["${" + param + "}"]


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, expected_asplcid, os, a_bid", [
        ("Interstitial Mirror Android - UMP WIN", 380991, 1539906510215, 381611, "Android", _A_BID_UMP_WIN),
        ("Interstitial Mirror iOS - UMP WIN", 380991, 1535339526186, 381611, "iOS", _A_BID_UMP_WIN),
        ("Interstitial Mirror Android - UMP LOSS", 380991, 1539906510215, 381611, "Android", _A_BID_UMP_LOSS),
        ("Interstitial Mirror iOS - UMP LOSS", 380991, 1535339526186, 381611, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_asplcid(plc_desc: str, as_plid: int, im_plid: int, expected_asplcid: int, os: str, a_bid: int):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        as_plid=as_plid,
        im_plid=im_plid,
        os=os,
        mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    asplcid = mutt_response.ad_sets[0].get("ads")[0].get("asPlcId")
    assert asplcid == expected_asplcid, \
        f"{plc_desc} -- expected {expected_asplcid}, but got {asplcid} instead"


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, os, a_bid", [
        ("Interstitial Mirror Android - UMP WIN", 381024, 1539906510215, "Android", 2 * _A_BID_UMP_WIN),
        ("Interstitial Mirror iOS - UMP WIN", 381024, 1535339526186, "iOS", 2 * _A_BID_UMP_WIN),
        ("Interstitial Mirror Android - UMP LOSE", 381024, 1539906510215, "Android", _A_BID_UMP_LOSS),
        ("Interstitial Mirror iOS - UMP LOSE", 381024, 1535339526186, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_tracking_pixels_in_pubcontent_static(plc_desc: str, as_plid: int, im_plid: int, os: str, a_bid: int):
    while True:
        mutt_response, _ = client.request(
            env=_ENV,
            adtype=AdType.INTERSTITIAL.value,
            as_plid=as_plid,
            im_plid=im_plid,
            os=os,
            mutt_payload={"a-bid": a_bid}
        )
        assert bool(mutt_response), "response shouldn't be empty"
        markup_type = mutt_response.ad_sets[0].get("ads")[0].get("markupType")
        if markup_type == "html" and mutt_response.ad_sets[0].ads[0].get("metaInfo").get("creativeType") == "nonvideo":
            break

    pub_content = str(mutt_response.ad_sets[0].get("ads")[0].get("pubContent"))
    if "UMP LOSE" in plc_desc:
        assert mutt_response.ad_sets[0].ads[0].get("transaction").get("buyerName") == "AerMarket"
        vte.check_UMP_beacon_in_AS_ads(pub_content)
        vte.check_pixelate_tracking_in_pubcontent(pub_content, "html")
    else:
        assert mutt_response.ad_sets[0].ads[0].get("transaction").get("buyerName") == "InMobi"
        vte.check_AS_tracking_events_in_UMP_ads(pub_content, "html", "nonvideo")


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, os, a_bid", [
        ("Interstitial Mirror Android - UMP WIN", 380991, 1539906510215, "Android", _A_BID_UMP_WIN),
        ("Interstitial Mirror iOS - UMP WIN", 380991, 1535339526186, "iOS", _A_BID_UMP_WIN),
        ("Interstitial Mirror Android - UMP LOSE", 380991, 1539906510215, "Android", _A_BID_UMP_LOSS),
        ("Interstitial Mirror iOS - UMP LOSE", 380991, 1535339526186, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_tracking_pixels_in_pubcontent_video(plc_desc: str, as_plid: int, im_plid: int, os: str, a_bid: int):
    while True:
        mutt_response, _ = client.request(
            env=_ENV,
            adtype=AdType.INTERSTITIAL.value,
            as_plid=as_plid,
            im_plid=im_plid,
            os=os,
            mutt_payload={"a-bid": a_bid}
        )
        assert bool(mutt_response), "response shouldn't be empty"
        markup_type = mutt_response.ad_sets[0].get("ads")[0].get("markupType")
        if mutt_response.ad_sets[0].ads[0].get("metaInfo").get("creativeType") == "video":
            break

    pub_content = str(mutt_response.ad_sets[0].get("ads")[0].get("pubContent"))
    if "UMP LOSE" in plc_desc:
        assert mutt_response.ad_sets[0].ads[0].get("transaction").get("buyerName") == "AerMarket"
        vte.check_UMP_beacon_in_AS_ads(pub_content)
        vte.check_pixelate_tracking_in_pubcontent(pub_content, "inmobiJson")
    else:
        assert mutt_response.ad_sets[0].ads[0].get("transaction").get("buyerName") == "InMobi"
        vte.check_AS_tracking_events_in_UMP_ads(pub_content, "inmobiJson", "video")


@pytest.mark.parametrize(
    "plc_desc, as_plid, im_plid, os, a_bid", [
        ("Interstitial Mirror Android - UMP WIN", 380991, 1539906510215, "Android", _A_BID_UMP_WIN),
        ("Interstitial Mirror iOS - UMP WIN", 380991, 1535339526186, "iOS", _A_BID_UMP_WIN),
        ("Interstitial Mirror Android - UMP LOSS", 380991, 1539906510215, "Android", _A_BID_UMP_LOSS),
        ("Interstitial Mirror iOS - UMP LOSS", 380991, 1535339526186, "iOS", _A_BID_UMP_LOSS),
    ]
)
def test_applyBitmap(plc_desc: str, as_plid: int, im_plid: int, os: str, a_bid: int):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        as_plid=as_plid,
        im_plid=im_plid,
        os=os,
        mutt_payload={"a-bid": a_bid}
    )
    assert bool(mutt_response), "response shouldn't be empty"
    apply_bitmap = mutt_response.ad_sets[0].get("ads")[0].get("applyBitmap")
    markup_type = mutt_response.ad_sets[0].get("ads")[0].get("markupType")
    if markup_type == "html":
        assert apply_bitmap == False, \
            f"{plc_desc} -- expected False, but got {apply_bitmap} instead"
    else:
        assert apply_bitmap is None, \
            f"{plc_desc} -- expected None, but got {apply_bitmap} instead"


#-------- close-auction=false --------


@pytest.mark.parametrize(
    "plc_desc, os, as_plid, im_plid, a_bid",
    [
        # bids price: $4, $5, $7, $10, $14
        ("Android - Winning UMP + AdColony + AppNext + RTB Vast", "Android", 381001, 1539906510215, micro(20)),
        # bids price: $5, $7, $10, $14, $20
        ("iOS - Losing UMP + AdColony + Vungle + RTB Vast", "iOS", 381003, 1535339526186, micro(15)),
    ]
)
def test_non_close_auction_response(
    plc_desc: str,
    os: str,
    as_plid: int,
    im_plid: int,
    a_bid: int,
):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.INTERSTITIAL.value,
        im_plid=im_plid,
        as_plid=as_plid,
        os=os,
        has_dynamic_mediation=True,
        mutt_payload={"a-bid": a_bid},
    )
    assert len(mutt_response) == 6, "Losing UMP should not be in the ad set"
    assert len(mutt_response.ad_sets) == 1, "there should be only 1 ad set"
    ad_set = mutt_response.ad_sets[0]
    assert set(ad_set.keys()) == {"adSetId", "expiry", "isAuctionClosed", "adSetAuctionMeta", "ads"}
    assert ad_set.get("adSetAuctionMeta") is not None
    for ad in mutt_response.ad_sets[0].ads:
        common_keys = {
            "pubContent",
            "tracking",
            "markupType",
            "expiry",
            "impressionId",
            "canLoadBeforeShow",
            "metaInfo",
            "transaction",
            "trackingEvents",
            "baseEventUrl",
            "asPlcId",
            "adAuctionMeta",
        }
        markup_type = ad.get("markupType")
        buyer = ad.get("transaction").get("buyerName")
        print(buyer)
        print(markup_type)
        expected_keys = common_keys
        if buyer == "InMobi":
            expected_keys = expected_keys.union({"creativeId"})
        else:
            expected_keys = expected_keys.union({"viewability"})
        if markup_type == "html":
            expected_keys = expected_keys.union({"applyBitmap"})

        assert set(ad.keys()) == expected_keys

        assert ad.get("adAuctionMeta") is not None
        transaction = ad.get("transaction")
        assert set(transaction.keys()) >= {"buyerName", "adSourceName"}
        assert "ctxHash" not in transaction and "buyerPrice" not in transaction
