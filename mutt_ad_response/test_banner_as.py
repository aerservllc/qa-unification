import re
import pytest
import mutt.request.request_mutt as mrp
from mutt import client
from mutt.common import AdType
from urllib.parse import parse_qs, urlparse
from collections import Counter


#-------- close-auction=true --------
@pytest.mark.parametrize(
    "plc_desc, plc", [
        ("Non-dynamic sdk mediation", 380025),
        ("S2S mediation", 380308),
        ("AerMarket RTB", 380427),
        ("Custom JS(House Ad)", 380512),
        ("Mraid(AerMarket Mediation)", 380677),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_all_keys_available(plc_desc: str, plc: int, os: str):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        as_plid=plc,
        os=os,
    )
    expected_root_keys = set(["requestId", "adSets"])
    expected_ad_set_keys = set(["adSetId", "isAuctionClosed", "expiry", "ads"])
    expected_ad_keys = set(
        [
            "pubContent", "tracking", "markupType", "expiry", "impressionId", "canLoadBeforeShow", "metaInfo",
            "transaction", "trackingEvents", "baseEventUrl", "asPlcId"
        ]
    )
    assert bool(mutt_response), "response shouldn't be empty"
    raw_response = mutt_response.raw_response()
    assert set(raw_response.keys()) == expected_root_keys
    adset = raw_response.get("adSets")[0]
    assert set(adset.keys()) == expected_ad_set_keys
    for ad in adset.get("ads"):
        assert set(ad.keys()) >= expected_ad_keys


@pytest.mark.parametrize(
    "plc_desc, plc, expected_is_auction_closed", [
        ("Non-dynamic sdk mediation", 380025, True),
        ("S2S mediation", 380308, True),
        ("AerMarket RTB", 380427, True),
        ("Custom JS(House Ad)", 380512, True),
        ("Mraid(AerMarket Mediation)", 380677, True),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_isAuctionClosed(plc_desc: str, plc: int, expected_is_auction_closed: bool, os: str):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        as_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    is_auction_closed = mutt_response.ad_sets[0].get("isAuctionClosed")
    assert is_auction_closed == expected_is_auction_closed, \
        f"{plc_desc} -- expected {expected_is_auction_closed}, but got {is_auction_closed} instead"


@pytest.mark.parametrize(
    "plc_desc, plc, expected_partner_code", [
        ("Non-dynamic sdk mediation", 380025, "aerservdisplay599566902305"),
        ("S2S mediation", 380308, "aerservdisplay599566902305"),
        ("AerMarket RTB", 380427, "aerservdisplay599566902305"),
        ("Custom JS(House Ad)", 380512, "aerservdisplay599566902305"),
        ("Mraid(AerMarket Mediation)", 380677, "aerservdisplay599566902305"),
        ("AerMarket Mobile Banner Moat Enabled", 380816, "aerservdisplay599566902305")
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_viewability(plc_desc: str, plc: int, expected_partner_code: str, os: str):
    """
    Logic for viewability internal enablement:
    Only AerMarket Mediation(BANNER or VAST) can have it True if:
     - ad_line.is_moat_enabled=True
     - rtb response has moat.com as a vendor
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        as_plid=plc,
        os=os,
    )
    expected_client_levels = []
    expected_client_slicers = []
    if plc == 380816:
        expected_client_levels.extend(["AerMarketRtbMobile", "1000", "380256", "380175"])
        expected_client_slicers.extend([f"sample {os} app".lower(), "380816"])
    assert bool(mutt_response), "response shouldn't be empty"
    viewability = mutt_response.ad_sets[0].get("ads")[0].get("viewability")[0]
    assert viewability, "viewability object should exist"
    moat = viewability.get("moat")
    assert moat.get("instrumentVideo") is False
    assert moat.get("enabled") is bool(expected_client_levels)
    assert moat.get("partnerCode") == expected_partner_code
    assert moat.get("clientLevels") == expected_client_levels
    assert moat.get("clientSlicers") == expected_client_slicers


@pytest.mark.parametrize(
    "plc_desc, plc", [
        ("Non-dynamic sdk mediation", 380025),
        ("S2S mediation", 380308),
        ("AerMarket RTB", 380427),
        ("Custom JS(House Ad)", 380512),
        ("Mraid(AerMarket Mediation)", 380677),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_tracking(plc_desc: str, plc: int, os: str):
    """
    Tracking type:
    Web: when serving video 3.0. Else, unknown.
    Only UMP supports video 3.0 as of now. AS is currently being served as video 2.0
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        as_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    tracking = mutt_response.ad_sets[0].get("ads")[0].get("tracking")
    assert tracking == "unknown", \
        f"{plc_desc} -- expected 'unknown', but got {tracking} instead"


@pytest.mark.parametrize(
    "plc_desc, plc, expected_markup_type", [
        ("Non-dynamic sdk mediation", 380025, "mediationJson"),
        ("S2S mediation", 380308, "html"),
        ("AerMarket RTB", 380427, "html"),
        ("Custom JS(House Ad)", 380512, "html"),
        ("Mraid(AerMarket Mediation)", 380677, "html"),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_markupType(plc_desc: str, plc: int, expected_markup_type: str, os: str):
    """
    The possible values that Mutt sends today are: html, inmobiJson and mediationJson,
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        as_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    markup_type = mutt_response.ad_sets[0].get("ads")[0].get("markupType")
    assert markup_type == expected_markup_type, \
        f"{plc_desc} -- expected {expected_markup_type}, but got {markup_type} instead"


@pytest.mark.parametrize(
    "plc_desc, plc", [
        ("Non-dynamic sdk mediation", 380025),
        ("S2S mediation", 380308),
        ("AerMarket RTB", 380427),
        ("Custom JS(House Ad)", 380512),
        ("Mraid(AerMarket Mediation)", 380677),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_expiry(plc_desc: str, plc: int, os: str):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        as_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    expiry = mutt_response.ad_sets[0].get("ads")[0].get("expiry")
    assert expiry == 600, "default value of expiry should be 600"


@pytest.mark.parametrize(
    "plc_desc, plc", [
        ("Non-dynamic sdk mediation", 380025),
        ("S2S mediation", 380308),
        ("AerMarket RTB", 380427),
        ("Custom JS(House Ad)", 380512),
        ("Mraid(AerMarket Mediation)", 380677),
    ]
)
def test_safeArea(plc_desc: str, plc: int):
    params = {
        "env": _ENV,
        "adtype": AdType.BANNER.value,
        "as_plid": plc,
        "os": "iOS",
        mrp.IOS_DEVICE_MACHINE_HARDWARE: "iPhone10,3",
    }
    mutt_response, _ = client.request(**params)
    assert bool(mutt_response), "response shouldn't be empty"
    safe_area = mutt_response.ad_sets[0].get("ads")[0].get("safeArea")
    assert safe_area is None, "safeArea shouldn't exist for banner ads"


@pytest.mark.parametrize(
    "plc_desc, plc", [
        ("Non-dynamic sdk mediation", 380025),
        ("S2S mediation", 380308),
        ("AerMarket RTB", 380427),
        ("Custom JS(House Ad)", 380512),
        ("Mraid(AerMarket Mediation)", 380677),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_canLoadBeforeShow(plc_desc: str, plc: int, os: str):
    """
    AerServ's Ad responses cannot be loaded in the webview before the show is invoked. But UMP's ads can.
    This flag determines if the ad response can be loaded in the webview before the show is invoked or not.
    UMP ad: true
    AerMarket ad: true for markupType != html, false otherwise
    Mediation ad: true for native and interstitial. false for banner
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        as_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    can_load_before_show = mutt_response.ad_sets[0].get("ads")[0].get("canLoadBeforeShow")
    assert can_load_before_show == False, \
        "canLoadBeforeShow should be False for all banner ads(type=3) and non-sdk html"


@pytest.mark.parametrize(
    "plc_desc, plc, expected_creative_type", [
        ("Non-dynamic sdk mediation", 380025, "unknown"),
        ("S2S mediation", 380308, "nonvideo"),
        ("AerMarket RTB", 380427, "nonvideo"),
        ("Custom JS(House Ad)", 380512, "nonvideo"),
        ("Mraid(AerMarket Mediation)", 380677, "nonvideo"),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_metaInfo(plc_desc: str, plc: int, os: str, expected_creative_type: str):
    """
    omidEnabled will be true if OMSDK needs to be initialized by our SDK (dictated by UMP demand).
    customReferenceData and isolateVerificationScripts are today sent as empty string and false,
    respectively from server side. These are also required by SDK for OMSDK init.
    Only supported for UMP. For AS, this will have "omidEnabled: false".
    Structure:
    {
    "omidEnabled": "true/false",
    "customReferenceData": "some-string",
    "isolateVerificationScripts": "true/false",
    "macros": {
        "key": "value",
        "key2": "value2"
    }
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        as_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    expected_metainfo_keys = set(["creativeType", "omsdkInfo"])
    meta_info = mutt_response.ad_sets[0].get("ads")[0].get("metaInfo")
    assert set(meta_info.keys()) == expected_metainfo_keys
    assert meta_info.get("creativeType") == expected_creative_type
    assert meta_info.get("omsdkInfo").get("omidEnabled") == False
    assert meta_info.get("omsdkInfo").get("isolateVerificationScripts") == False


@pytest.mark.parametrize(
    "plc_desc, plc, expected_key_value", [
        (
            "Non-dynamic sdk mediation", 380025, {
                "buyerPrice": 1,
                "adSourceName": "iOS ADMOB BANNER NO FILL",
                "buyerName": "AdMob",
            }
        ),
        (
            "S2S mediation", 380308, {
                "buyerPrice": 50,
                "adSourceName": "MobFox Banner Ad Source ",
                "buyerName": "Mobfox",
            }
        ),
        (
            "AerMarket RTB", 380427, {
                "buyerPrice": 10,
                "adSourceName": "AerMarket Test Page",
                "buyerName": "AerMarket",
                "dspId": "DSPMock",
                "adomain": "aerserv.com",
            }
        ),
        (
            "Custom JS(House Ad)", 380512, {
                "buyerPrice": 1,
                "adSourceName": "AerMarket Test Page",
                "buyerName": "AerMarket",
            }
        ),
        (
            "Mraid(AerMarket Mediation)", 380677, {
                "buyerPrice": 5,
                "adSourceName": "AerMarket Test Page",
                "buyerName": "AerMarket",
            }
        ),
    ]
)
def test_transaction(plc_desc: str, plc: int, expected_key_value: dict):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        as_plid=plc,
        os="iOS",
    )
    expected_transaction_keys = set(expected_key_value.keys())
    expected_transaction_keys.add("ctxHash")
    assert bool(mutt_response), "response shouldn't be empty"
    transaction = mutt_response.ad_sets[0].get("ads")[0].get("transaction")
    assert set(transaction.keys()) == expected_transaction_keys, \
        f"{plc_desc} -- keys mismatch in transactions"
    for key in expected_key_value:
        expected_value = expected_key_value.get(key)
        result_value = transaction.get(key)
        assert expected_value == result_value, \
            f"{plc_desc} -- expected {expected_value}, but got {result_value} instead"


@pytest.mark.parametrize(
    "plc_desc, plc, expected_tracking_ev_types",
    [
        ("Non-dynamic sdk mediation", 380025, ["sdk_impression", "sdk_failure", "sdk_attempt"]),
        ("S2S mediation", 380308, ["vast_impression", "banner_rendered"
                                   ]),  # the reason it has vast_impression and banner_rendered is it is Mraid
        ("AerMarket RTB", 380427, ["banner_rendered"]),
        ("Custom JS(House Ad)", 380512, ["banner_rendered"]),
        ("Mraid(AerMarket Mediation)", 380677, [
            "vast_impression", "banner_rendered"
        ]),  # the reason it has vast_impression and banner_rendered is it is Mraid
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_tracking_events(plc_desc: str, plc: int, expected_tracking_ev_types: list, os: str):
    """
    380038 and 380677 have both vast_impression and banner_rendered because it is Mraid
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        as_plid=plc,
        os=os,
    )
    expected_tracking_events_keys = ["type", "trackingUrls"]
    expected_tracking_params = [
        "plc", "rid", "pubid", "appid", "gdpr", "gdpr_consent", "os", "adid", "sdkv", "iline", "asplcid", "asid",
        "pinned", "vc_user_id", "hck", "xcid", "ntid", "ctxhash", "eu", "ev"
    ]
    optional_tracking_params = ["rtbdspid", "rtbbidid", "rtbrid", "campaignid", "buyer", "icid", "pchain"]
    assert bool(mutt_response), "response shouldn't be empty"
    tracking_events = mutt_response.ad_sets[0].get("ads")[0].get("trackingEvents")
    assert len(tracking_events) == len(expected_tracking_ev_types)
    for tracking in tracking_events:
        assert set(tracking.keys()) ==set(expected_tracking_events_keys)
        assert tracking.get("type") in expected_tracking_ev_types
        tracking_params = parse_qs(urlparse(tracking.get("trackingUrls")[0]).query, True)
        assert all([len(value) == 1 for value in tracking_params.values()]), \
            "there shouldn't be any duplicate keys"
        extra_keys = set(tracking_params.keys()).difference(set(expected_tracking_params))
        optional_keys = extra_keys.issubset(set(optional_tracking_params))
        assert len(extra_keys) == 0 or optional_keys


@pytest.mark.parametrize(
    "plc_desc, plc", [
        ("Non-dynamic sdk mediation", 380025),
        ("S2S mediation", 380308),
        ("AerMarket RTB", 380427),
        ("Custom JS(House Ad)", 380512),
        ("Mraid(AerMarket Mediation)", 380677),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_base_event_url(plc_desc: str, plc: int, os: str):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        as_plid=plc,
        os=os,
    )
    expected_parameters_in_base_event_url = [
        "plc", "rid", "pubid", "appid", "gdpr", "gdpr_consent", "os", "adid", "sdkv", "iline", "asplcid", "asid",
        "campaignid", "pinned", "vc_user_id", "buyer", "hck", "icid", "xcid", "ntid", "ctxhash", "eu", "ev", "pchain"
    ]
    assert bool(mutt_response), "response shouldn't be empty"
    base_event_url = mutt_response.ad_sets[0].get("ads")[0].get("baseEventUrl")
    event_params = parse_qs(urlparse(base_event_url).query, True)
    assert Counter(event_params.keys()) == Counter(expected_parameters_in_base_event_url)
    for param in ["ctxhash", "eu", "ev"]:
        assert event_params[param] == ["${" + param + "}"]


@pytest.mark.parametrize(
    "plc_desc, plc, expected_asplcid", [
        ("Non-dynamic sdk mediation", 380025, 380050),
        ("S2S mediation", 380308, 380536),
        ("AerMarket RTB", 380427, 380617),
        ("Custom JS(House Ad)", 380512, 380707),
        ("Mraid(AerMarket Mediation)", 380677, 380885),
    ]
)
def test_asplcid(plc_desc: str, plc: int, expected_asplcid: int):
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        as_plid=plc,
        os="iOS",
    )
    assert bool(mutt_response), "response shouldn't be empty"
    asplcid = mutt_response.ad_sets[0].get("ads")[0].get("asPlcId")
    assert asplcid == expected_asplcid, \
        f"{plc_desc} -- expected {expected_asplcid}, but got {asplcid} instead"


@pytest.mark.parametrize(
    "plc_desc, plc, expected_apply_bitmap", [
        ("Non-dynamic sdk mediation", 380025, None),
        ("S2S mediation", 380308, False),
        ("AerMarket RTB", 380427, False),
        ("Custom JS(House Ad)", 380512, True),
        ("Mraid(AerMarket Mediation)", 380677, False),
    ]
)
@pytest.mark.parametrize("os", ["Android", "iOS"])
def test_applyBitmap(plc_desc: str, plc: int, expected_apply_bitmap: bool or None, os: str):
    """
    Always False for UMP or video ads.
    For AS banner ads, 'applyBitmap'=True only when 'canFailover'=True
    """
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=AdType.BANNER.value,
        as_plid=plc,
        os=os,
    )
    assert bool(mutt_response), "response shouldn't be empty"
    apply_bitmap = mutt_response.ad_sets[0].get("ads")[0].get("applyBitmap")
    assert apply_bitmap == expected_apply_bitmap, \
        f"{plc_desc} -- expected {expected_apply_bitmap}, but got {apply_bitmap} instead"
