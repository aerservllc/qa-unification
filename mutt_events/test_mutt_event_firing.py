import uuid
import time
import datetime
import pytest
import mutt.request.request_mutt as mrp
from mutt import client
from mutt.common import AdType
from mutt.config import environment as mutt_env
from aerserv import adserver
from aerserv.config.aerserv import environment as aerserv_env
from aerserv.adserver.events import Event, ServerSideEvents, ErrorEvents, ClientSideEvents
from aerserv.log_fetcher import logs
from aerserv import whitelist_params
from mutt.request.request_mutt import micro

_REQUEST_ATTEMPTS = 10


def assert_events_in_log(log_bucket: logs.Logs, expected_entries: set, log_path: str, adid: str):
    today = datetime.datetime.utcnow().date().isoformat()
    as_endpoint = mutt_env.get(_ENV, "AdServer", "as_endpoint")
    event_path = aerserv_env.get(as_endpoint, "AdServer", log_path)
    log_entries = log_bucket.get_from_path(f"{event_path}ds={today}")

    print(f"search for adid: {adid}")
    received_entries = set(
        map(
            lambda x: Event[x.event_type],
            filter(
                lambda x: adid == x.advertising_id,
                filter(lambda record: record.is_record_valid(), map(adserver.Record, log_entries))
            )
        )
    )
    print(f"received: {received_entries}\nexpected: {expected_entries}")
    assert received_entries >= expected_entries, f"not all expected events are in {log_path}"


def flush_event_logs(attempts: int):
    # Flush the logs with 6 MB of events, kinda
    for i in range(attempts):
        response = client.request(
            env=_ENV,
            adtype=AdType.BANNER.value,
            as_plid=380427,
            os="Android",
            silent=True,
        )


@pytest.mark.flaky
@pytest.mark.parametrize(
    "asplid, implid, adtype, os, has_dynamic_mediation, expected_events", [
        (
            380958, None, AdType.BANNER.value, "Android", False,
            set([Event.mutt_as_attempt, Event.mutt_as_impression, Event.mutt_win, Event.mutt_lost_on_price])
        ),
        (
            380992, 1537235559083, AdType.BANNER.value, "iOS", False,
            set(
                [
                    Event.mutt_as_attempt, Event.mutt_as_impression, Event.mutt_im_attempt, Event.mutt_im_impression,
                    Event.mutt_win, Event.mutt_lost_on_price
                ]
            )
        ),
        (
            380993, None, AdType.INTERSTITIAL.value, "Android", False,
            set([Event.mutt_as_attempt, Event.mutt_as_impression, Event.mutt_win, Event.mutt_lost_on_price])
        ),
        (
            380991, 1535339526186, AdType.INTERSTITIAL.value, "iOS", False,
            set(
                [
                    Event.mutt_as_attempt, Event.mutt_as_impression, Event.mutt_im_attempt, Event.mutt_im_impression,
                    Event.mutt_win, Event.mutt_lost_on_price
                ]
            )
        ),
        (
            380988, None, AdType.INTERSTITIAL.value, "Android", True,
            set([Event.mutt_as_attempt, Event.mutt_as_impression, Event.mutt_win, Event.mutt_lost_on_price])
        ),
        (
            381000, 1535339526186, AdType.INTERSTITIAL.value, "iOS", True,
            set(
                [
                    Event.mutt_as_attempt, Event.mutt_as_impression, Event.mutt_im_attempt, Event.mutt_im_impression,
                    Event.mutt_win, Event.mutt_lost_on_price
                ]
            )
        ),
    ]
)
def test_mutt_events(
    cache_client: pytest.fixture,
    log_bucket,
    asplid: int,
    implid: int or None,
    adtype: str,
    os: str,
    has_dynamic_mediation: bool,
    expected_events: set,
):
    adid = str(uuid.uuid4())
    cache_key = f"{asplid}_{adid}_{whitelist_params.AD_CACHING_METRO_CODE}"
    print(cache_key)
    cache_client.delete(cache_key)

    mutt_payload = {"a-bid": micro(20)} if asplid == 381000 else {}
    mutt_response, _ = client.request(
        env=_ENV,
        adtype=adtype,
        as_plid=asplid,
        im_plid=implid,
        os=os,
        adid=adid,
        has_dynamic_mediation=has_dynamic_mediation,
        mutt_payload=mutt_payload,
    )

    assert bool(mutt_response), "response shouldn't be empty"
    flush_event_logs(_REQUEST_ATTEMPTS)
    time.sleep(_SLEEP_TIME)
    assert_events_in_log(log_bucket, expected_events, "log_events", adid)
