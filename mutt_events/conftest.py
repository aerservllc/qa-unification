from aerserv.log_fetcher import logs
from mutt.config import environment as mutt_env
import pytest
import time


@pytest.fixture(scope="module", autouse=True)
def log_bucket(request):
    as_endpoint = mutt_env.get(request.config.getoption('--env'), "AdServer", "as_endpoint")
    s3_bucket = logs.Logs(as_endpoint, "AdServer", "log_bucket")

    def clearup():
        s3_bucket.clear("log_events")
        time.sleep(int(request.config.getoption("--sleep-time")))

    clearup()
    request.addfinalizer(clearup)
    return s3_bucket
