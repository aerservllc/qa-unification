import pytest
from mutt.config import environment as mutt_env
from aerserv.adserver.ad_cache import AdCache


def pytest_addoption(parser):
    # Specify test environment
    parser.addoption("--env", action="store", default="local", help="Options: local pre_prod_dev pre_prod_staging prod")
    parser.addoption(
        "--sleep-time", action="store", default="3", help="Options: sleep time after cleaning s3 event logs"
    )


@pytest.fixture(autouse=True)
def env(request):
    request.function.__globals__['_ENV'] = mutt_env.from_string(request.config.getoption("--env"))
    request.function.__globals__['_SLEEP_TIME'] = int(request.config.getoption("--sleep-time"))


def pytest_generate_tests(metafunc):
    idlist = []
    argvalues = []
    try:
        for scenario in metafunc.cls.scenarios:
            idlist.append(scenario[0])
            items = scenario[1].items()
            argnames = [x[0] for x in items]
            argvalues.append(([x[1] for x in items]))
    except (AttributeError):
        return
    metafunc.parametrize(argnames, argvalues, ids=idlist, scope="class")


@pytest.fixture(autouse=True)
def cache_client(request):
    env = mutt_env.from_string(request.config.getoption("--env"))
    as_env = mutt_env.get(env, "AdServer", "as_endpoint")
    return AdCache(as_env)
