# APPLY YAPF

```
$ bin/setup_yapf
```

This will download and set up the pre-commit for your current repo. Run this script once for all.

The git pre-commit hook automatically formats your Python files before they are committed to your local repository. Any changes yapf makes to the files will stay unstaged so that you can diff them manually.


# Architecture

Please refer to [this doc](https://docs.google.com/document/d/1eBFtXrALdLJpwa1crH9Y5bnx3nR-F5anG35x_azrg88/edit#)


# AD_RESPONSES

auction closed: meaning AS plc doesnt have dynamic sdk ad sources.

Please refer to [this doc](https://docs.google.com/spreadsheets/d/1MsAWgmc_oxAcEdkS-mL06fO3Y6GjKBo9hVkAXFkeaWM/edit#gid=567426986) for the contracts among SDK, Mutt, UMP, Ad Server, and UA(Unified Auction)


Please take a moment to familiarize yourself with the responses below:

** when auction is closed, only Mutt Request:
  
```
curl -X POST -H "Content-Type:application/x-www-form-urlencoded" -H "x-forwarded-for:71.190.79.14" -H "user-agent:Mozilla/5.0 (Linux; Android 7.0; SM-G892A Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/60.0.3112.107 Mobile Safari/537.36" -d 'adtype=banner&tz=-25200000&u-latlong-accu=35.0%2C-100&d-localization=en-US&s-cn=Verizon&format=unifiedSdkJson&mk-ad-slot=320x50&d-device-screen-size=320x667&u-appbid=com.aerserv.sample&vc_user_id=5e02d73b-3ce6-4a87-8c0c-1fe24fbfc650&d-model-name=Nexus&mk-version=pr-SAND-HTBTB-20180827&client-request-id=ceaf49a2-f945-4af0-bc67-cc6294371de8&a-bid=20000000&as-ext=%7B%22coppa%22%3A+false%2C+%22pubGdprDpaSigned%22%3A+true%2C+%22applyGdprAgeOfConsent%22%3A+false%7D&u-id-adt=0&as-plid=380992&has-dynamic-mediation=false&d-orientation=1&mk-ads=1&consentobject=%7B%22gdpr_consent_available%22%3A+true%7D&im-ext=%7B%22coppa%22%3A+false%2C+%22pubGdprDpaSigned%22%3A+true%2C+%22applyGdprAgeOfConsent%22%3A+false%7D&u-id-map=%7B%22GPID%22%3A+%225e02d73b-3ce6-4a87-8c0c-1fe24fbfc650%22%7D&im-plid=1539853098877' -b "" "https://stg-ads.inmobi.com/sdk"
```

** When auction is not closed, there will be a Mutt request and a UA request:

Mutt request:
    
```
curl -X POST -H "Content-Type:application/x-www-form-urlencoded" -H "x-forwarded-for:71.190.79.14" -H "user-agent:Mozilla/5.0 (Linux; Android 7.0; SM-G892A Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/60.0.3112.107 Mobile Safari/537.36" -d 'client-request-id=f154483d-5eb8-4bc5-8411-aaadeb651b04&tz=-25200000&d-device-screen-size=320x667&d-model-name=Nexus&adtype=int&u-latlong-accu=35.0%2C-100&d-localization=en-US&u-appbid=com.aerserv.sample&s-cn=Verizon&vc_user_id=bdae9e66-e801-457b-aa71-16f639692e00&format=unifiedSdkJson&mk-version=pr-SAND-HTBTB-20180827&d-orientation=1&as-plid=380999&mk-ads=1&a-bid=20000000&u-id-map=%7B%22GPID%22%3A+%22bdae9e66-e801-457b-aa71-16f639692e00%22%7D&has-dynamic-mediation=true&u-id-adt=0&im-ext=%7B%22coppa%22%3A+false%2C+%22pubGdprDpaSigned%22%3A+true%2C+%22applyGdprAgeOfConsent%22%3A+false%7D&im-plid=1539906510215&consentobject=%7B%22gdpr_consent_available%22%3A+true%7D&as-ext=%7B%22coppa%22%3A+false%2C+%22pubGdprDpaSigned%22%3A+true%2C+%22applyGdprAgeOfConsent%22%3A+false%7D' -b "" "https://stg-ads.inmobi.com/sdk"
```
    
UA Request:
    
```
curl -X POST -H "Content-Type:application/json" -H "x-forwarded-for:71.190.79.14" -H "user-agent:Mozilla/5.0 (Linux; Android 7.0; SM-G892A Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/60.0.3112.107 Mobile Safari/537.36" -d '{"requestId": "f154483d-5eb8-4bc5-8411-aaadeb651b04", "adType": "int", "ads": [{"impressionId": "0ee8f1ef-b5a4-4208-b3ba-4b40935fa8a3", "adAuctionMeta": "2:8rusGz01ohuZwb6bkDlLoA==:jgz6/8F2FdBAep81LGjHpFbDlTxbJiE5PIeg6e8pMBrELX13HlUYrM5Lopku0ODoUCgiXN4kMZwNIn6I6cCmo3D500+2TCpDQwWeKx2O86Ap/wUBQQt4j5pTmaoGr9VEPm5UCbrgWcv4M7+ugB/uSlyQi964KcwGhgj85IS48hOEh/JIoC6ZmMidOK3E7ovd10lxUFiG9kGL6lY9HSElTs3DSq9MbuK03tNgdiFFmu7IKfdvPS3HSpLxvDLovtP7sNkigViDU/3p6yPuiXcJ3mydji/Y31IBk2luqpJbcY/mbMYjSbYoXYglFH0nIe1ivpxF8GGiTXDvT+02ckx5ntZuPe1mCXPORUCtwJrwsdh75rSKMd57RZGlzOuFq3J5HiCet/pYyXGuT+FL3bYWdvMc1DPBsIrU/+jTnmco2A4re7ZGFx6edYIvWYl0jjivCk1l0i3N0AEKVME8qNSw2IMb4Sjp4+25U72ScMcEmmP+AQUaqjMChusu90xY+7L2hOa6Hc0p9E7fm/QGSCqD6g=="}, {"impressionId": "0ee8f1ef-b5a4-4208-b3ba-4b40935fa8a3", "adAuctionMeta": "2:FrpkY9wEeijm03OkcCF1YQ==:fsMxrbFUt1ySlSYAGcCcxBanCUqzOtxRsNM57g8oz6uGkrj5ujSAjXvP9kgWh+e38xFKy0SFwB7268Ppn2FjCzNLsH9EIeLZRiLzyHx0mJ3WyeRMhAjBDFrYaTy55kjJMIWXJ5vGjfp0iSSDihV0sRVDjn41AnxiJoJ/saQEq2cMFQg1qiowgQCFdhXqSQAkCU+jEq92TQHEA/RvKGW4EuPVqhjs70aDVyIbXSSlsUcMAmzMZfSnqPwisvX2dzc+fo93VXRq3k7nZdrcWYe/q8gIpGsiCje2RCM7zGZdI63Izm8bdmaOcIbtSl0ScJjh+/ceWJZtTf/dEHCB/1+sjXkubrUb9EweyR/Y0zcmKkOnClwzy+OKXovKmZRyFRIzQ82iV5JD1Jdor6FP2eS/otttk3SXwWXMwlpz16CPwrUXRuVr6sboILCbZKVDb7D4auiH6vXJQD2/5gEnZxe0wBLaPJ/z6Mhb+e9+0GYk6s3FaMMnGzkNHrL0agBvWpGyJSE6/4g+2RULSMecXOnehb/u7kBN4i5Y3db1Og1bZbA="}, {"impressionId": "0ee8f1ef-b5a4-4208-b3ba-4b40935fa8a3", "adAuctionMeta": "2:6tWy8bPX5QY5MPSI/AUorg==:OWWTv6srHX5s4E/1wDlIXbL7X92N1inaZ81cbiDL022td5Kekiv3aCsEzDkr6nU77VVqYNjDyLGRplonStODRh3mi4P0V4IQ8z1Ky0bkWgVtK2IgP9CNKob7BQp5DqaqyeV5Gpybr742ICn0SOomlQvztLQLoEEiGnVf6diHSpyRmUDPY8c2iLjXF9tiYEpc5bJgauip1IH+47MaEmx6gCcdVWlhZiQYnQzBO7R75LINljbN6WwziifMsY0aIF3JGsnX1ibFbrY3DQH6Pm9fbQlRy1b/F/s3qfN/Aeh48CdoP7PCnrESUcjOmInbz532bYqUvSz8eNCvSB+/hbJmgRw98gVE47siNpaiVgzEZN6NnM/2HAqJTgssBozNQ+a1xYvzYn+RymRpm9Nq7Qq3iU9Nqlt6LQllydXbJFuH5+Q0Tu1kKZI5m1i+iCnG166ZN+C1a0L+3I9sl5eAijRdAiGEFHf2OKoH0yXh2agsVVpNz6/FEuWgOkohMNUIw9gDs1iL3Jd87CGJ/O8dE84Tw4I/gNS5dT0Ks0/OLY5TCHmk6lsPuYrCK3J8ApC5QX+gWRiE8DP7WjKa1gUTiucB1JvumVuRBCfpabsqPB5W0oA="}], "adSetAuctionMeta": "2:H+nM2OmltNAW1wbZVt/hCQ==:Wbj5IMRwjZQUyYqwNcVvUq27MrdMaazzGELvOfmTIDoMjD2vR2Q/aCUYR6n4Qe1AkDNpvAl8jFkzFg0uTovKcrDDREiV11avmnZs2bNpqNikJrMzjX6geVOX6G74WoPdejyVz8o6r/+l8YOy9PHTUn9wQOs1DFFsyBmauXnhYi36aCwVbSZibHMMDQU6CXkymXpQKCJ5ABDMonpgnDN95zGqnioO+GmepJWS9Dk3Te1d1WL1FlSLJmr7JAStkPMS"}' -b "" "https://stg-ua.inmobi.com/sdk/ua/"
```

** The reason we have `adSetAuctionMeta` and `adAuctionMeta` is: we want to pass down price info to Unified Auction for the final auction, but we also want to save bandwidth. SDK gets what Mutt passes to it then resends it to UA. UA server will have the encryption key to decrypt what it receives from SDK, along with the new price info it gets from Ad server, it will make the final auction and return the winning ads.



Some attributes worth your extra attention:

| Attribute Name | Value&nbsp;&nbsp;&nbsp; | Note&nbsp;&nbsp;&nbsp;|
|--- | --- | --- |
|adSetAuctionMeta| a string of encrypted json objects with raw pricing info of losing ads| Only exists when auction not closed. Include THE losing UMP ad or/AND an AS ad serving as the base price for second pricing|
|viewability|Refer to [this ticket](https://aerserv.atlassian.net/browse/DEV-5021)|-|
|markupType|`mediationJson`: for s2s and sdk ads. <br>`inmobiJson`: for UMP or AS AerMarket Video ads. <br>`html`: for aerbanner or interstitial static ads|-|
|canLoadBeforeShow|AerServ's Ad responses cannot be loaded in the webview before the show is invoked. But UMP's ads can. <br>This flag determines if the ad response can be loaded in the webview before the show is invoked or not. <br>UMP ad: true.<br> AerMarket ad: true for markupType != html, false otherwise. <br>Mediation ad: true for native and interstitial. false for banner|-|
|transaction|`buyerPrice` and `ctxHash` only exist when auction closed. <br>`dspId` and `adomain` only exist when the ad is from RTB demand side|-|
|asPlcId|Aerserv asplcid|if this is a UMP only plc, this key would not exist|
|adAuctionMeta|a string of encryption of a winning ad(top 4) | only exist when auction not closed|
|applyBitmap|Always False for UMP or video ads.<br>For AS banner ads, 'applyBitmap'=True only when 'canFailover'=True|-|
|canFaiOver(within ad server response)|For banner, only custom JS and SDK will have canFailover True<br>For interstitial, if it is video, 'canFailover' will always be True, if it is banner, only custom JS and SDK will have it True|-|


# AUCTION

Please refer to [this doc Tab: Regression - Auction](https://docs.google.com/spreadsheets/d/1SoQVHA49jqT5LirepKWXW5i4ig9P4hpmLR5sneWIZnw/edit#gid=127647400)

@Daniel is the go-to guy for this matter.

By and large, we want to sort ads by publisher payout, then how much we can make.

Please refer to [qa-tests/README Auction session](https://bitbucket.org/aerservllc/qa-tests/src/master/)

As of today(12/26/2018), the only difference between Mutt Auction and AS Auction is:
[I'll ask @Daniel to fill this in]


# MUTT EVENTS(Logging)

Here is the list of events Mutt/UA fires:
* VIDEO0(2),
* VIDEO25(3),
* VIDEO50(4),
* VIDEO75(5),
* VIDEO100(6),
* CLICK(7),
* VAST_IMPRESSION(18),
* BANNER_PIXEL_FIRED(22),
* MUTT_IM_ATTEMPT(83),
* MUTT_IM_IMPRESSION(84),
* MUTT_AS_ATTEMPT(85),
* MUTT_AS_IMPRESSION(86),
* MUTT_WIN(87),
* MUTT_LOST_ON_PRICE(88),
* SDK_TRUE_REQUEST(89),
* SDK_TRUE_FILL(90);

Those events with the name `Mutt_xxx` are unique events only fired by Mutt/UA.
If there is a `implid` in the request, Mutt will fire `MUTT_IM_ATTEMPT`. If UMP returns an ad, Mutt will fire `MUTT_IM_IMPRESSION`.
If there is a `asplid` in the request, Mutt will fire `MUTT_AS_ATTEMPT`. If AerServ returns ad(s), Mutt will fire `MUTT_AS_IMPRESSION`.

`MUTT_WIN` and `MUTT_LOST_ON_PRICE` are pretty much self-explanatory.

Test strategy for logging test is the same as AS logging test, please refer to [qa-systems-test/README](https://bitbucket.org/aerservllc/qa-systems-test/src/master/)


# UNIFIED AUCTION
For UA price testing, refer to [this](https://docs.google.com/spreadsheets/d/1SoQVHA49jqT5LirepKWXW5i4ig9P4hpmLR5sneWIZnw/edit#gid=239346780)

@Max is the go-to guy for more detailed explanation.
